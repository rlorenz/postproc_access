#!/bin/bash
## \file post_proc_ACCESS_GLACE-1.sh
#  \author Ruth Lorenz (r.lorenz@unsw.edu.au)
#  \brief create timeseries for UM output which is archived on mdss as monthly *.nc.gz files
#  \usage qsub -V -v -lother=mdss post_proc_ACCESS_GLACE-1.sh

#PBS -q copyq
#PBS -N postpr_G1
#PBS -j oe
#PBS -l other=mdss
#PBS -l ncpus=1
#PBS -l mem=1536mb
#PBS -l walltime=01:30:00
#PBS -l wd
#PBS -P dt6

##-----------------------
# function to determine if year is leapyear (1) or not (0)
##-----------------------

function leapyr ()
{ 
  if [[  ${year}%4 -eq "0" && ${year}%100 -ne "0" || ${year}%400 -eq "0" ]]; then
    local __lpyr=1
  else
    local __lpyr=0
  fi
  echo "$__lpyr"
}

##-----------------------##
## load required modules ##
##-----------------------##

module load netcdf
module load nco
module load cdo
 
##---------------------##
## user specifications ##
##-------------------- ##
runexp=uaob
ensemble="${runexp}a ${runexp}b ${runexp}c ${runexp}d ${runexp}e ${runexp}f ${runexp}g ${runexp}h ${runexp}i ${runexp}j ${runexp}k ${runexp}l ${runexp}m ${runexp}n ${runexp}o ${runexp}p"
echo "Ensemble members are: ${ensemble}"
year_start=2000
year_end=2000
season=JJA
archive=$USER/
workdir=/short/dt6/$USER/postproc/$runexp/work/
outdir=/short/dt6/$USER/postproc/$runexp/timeseries/

##---------------------##

mkdir /short/dt6/$USER/postproc
mkdir /short/dt6/$USER/postproc/$runexp
mkdir ${outdir}
mkdir ${workdir}

cd ${workdir}

#loop over ensemble members
for exp in $ensemble
do
##-------------------------------------------##
## monthly files:                            ##
##-------------------------------------------##

## get data from mdss, loop over year, unzip
## and concatenate yearly archives          

year=$year_start
echo "Processing experiment $exp"
       if [ $season == "JJA" ]; then
	   months="06 07 08"
	   elif [ $season == "DJF" ]; then
	   months="12 01 02"
	   else echo "ERROR: Wrong season!"
	   exit 0
	   fi
		for month in $months
		  do       
		  #monthly files, check availability first
                  file_mon=`mdss dmls -l ${archive}/$exp/$exp.monthly.$year-$month-00T00.nc.gz | awk '{print $2}'`
                  if [[ "$file_mon" != 1 ]]; then
        	   echo "ERROR: missing file $exp.monthly.$year-$month-00T00.nc.gz in ${archive}" 1>&2
         	   exit 1
                  fi
		  mdss get ${archive}/$exp/$exp.monthly.$year-$month-00T00.nc.gz
		  gunzip $exp.monthly.$year-$month-00T00.nc.gz

		  if [ $month == "12" ]; then
		      let year=year+1
		      fi

		  done
		
echo "Create monthly timeseries..."
ncrcat -O $exp.monthly.????-??-00T00.nc ${workdir}/$exp.monthly_TS.${season}_${year_end}.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"ACCESS1.3b model output from GLACE-1 experiment\n" $exp.monthly_TS.${season}_${year_end}.nc
ncatted -h -O -a institution,global,a,c,"ARC Centre of Excellence for Climate System Science, model run at NCI National Facility\n" $exp.monthly_TS.${season}_${year_end}.nc
ncatted -h -O -a source,global,a,c,"ACCESS1.3b (HadGEM3-CABLE2.0), AMIP, N96\n" $exp.monthly_TS.${season}_${year_end}.nc
ncatted -h -O -a  contact,global,a,c,"r.lorenz@unsw.edu.au \n" $exp.monthly_TS.${season}_${year_end}.nc

# clean up
rm $exp.monthly.????-??-00T00.nc

##-------------------------------------------##
## daily files:                              ##
##-------------------------------------------##

  year=$year_start
       if [ $season == "JJA" ]; then
	   months="06 07 08"
	   elif [ $season == "DJF" ]; then
	   months="12 01 02"
	   fi

		for month in $months
		  do
		  file=`mdss dmls -l ${archive}/$exp/$exp.daily.$year-$month-00T00.nc.gz | awk '{print $2}'`
		  if [[ "$file" != 1 ]]; then
		      echo "ERROR: missing file $exp.daily.$year-$month-00T00.nc.gz in ${archive}" 1>&2
		      exit 2
		  fi
		  mdss get ${archive}/$exp/$exp.daily.$year-$month-00T00.nc.gz
		  gunzip $exp.daily.$year-$month-00T00.nc.gz

		  if [ $month == "12" ]; then
		      let year=year+1
		      fi

		  done #month

 # Create timeseries
		echo "Create daily timeseries..."
		ncrcat -O $exp.daily.????-??-00T00.nc $exp.daily.${season}_${year_end}.nc
		#add global attributes
		ncatted -h -O -a title,global,a,c,"ACCESS1.3b model output from GLACE-1 experiment\n" $exp.daily.${season}_${year_end}.nc
		ncatted -h -O -a institution,global,a,c,"ARC Centre of Excellence for Climate System Science, model run at NCI National Facility\n" $exp.daily.${season}_${year_end}.nc
		ncatted -h -O -a source,global,a,c,"ACCESS1.3b (HadGEM3-CABLE2.0), AMIP, N96\n" $exp.daily.${season}_${year_end}.nc
		ncatted -h -O -a  contact,global,a,c,"r.lorenz@unsw.edu.au \n" $exp.daily.${season}_${year_end}.nc

#clean up
		rm $exp.daily.????-??-00T00.nc

#loop over variables Tmean, Tmax,Tmin and PR
		for var in T TN TX PR CPR
		do
#prepare daily Tmean, Tmax,Tmin and PR
		    if [ $var == "T" ]; then
			variablename=tas
			ncks -O -v $variablename ${exp}.daily.${season}_${year_end}.nc ${exp}_${var}_${season}_${year_end}.nc

			elif [ $var == "TN" ]; then
			variablename=tasmin
			ncks -O -v $variablename ${exp}.daily.${season}_${year_end}.nc ${exp}_${var}_${season}_${year_end}.nc

			elif [ $var == "TX" ]; then
			variablename=tasmax
			ncks -O -v $variablename ${exp}.daily.${season}_${year_end}.nc ${exp}_${var}_${season}_${year_end}.nc

			elif [ $var == "PR" ]; then
			variablename=pr
			ncks -O -v $variablename ${exp}.daily.${season}_${year_end}.nc ${exp}_${var}_${season}_${year_end}.nc

			elif [ $var == "CPR" ]; then
			variablename=prc
			variablename1=prc1
			variablename2=prc2

			ncks -O -v $variablename1 ${exp}.daily.${season}_${year_end}.nc tmp1_${exp}.daily.${season}_${year_end}.nc
			ncks -O -v $variablename2 ${exp}.daily.${season}_${year_end}.nc tmp2_${exp}.daily.${season}_${year_end}.nc
			cdo add tmp1_${exp}.daily.${season}_${year_end}.nc tmp2_${exp}.daily.${season}_${year_end}.nc ${exp}_${var}_${season}_${year_end}.nc
			rm tmp*
			fi

  #prepare files for GLACE coupling strength, disregard first 8 (JJA), 6 (DJF) days, calculate 6 day means (T)/totals (P)
		    if [ $season == "JJA" ]; then
			ncks -d time,8, ${exp}_${var}_${season}_${year_end}.nc ${exp}_${var}_${season}_${year_end}_84days.nc
		    elif [ $season == "DJF" ]; then
			let t=6+$(leapyr)
			ncks -d time,$t, ${exp}_${var}_${season}_${year_end}.nc ${exp}_${var}_${season}_${year_end}_84days.nc
		    fi
		    if [ $var == "PR" ] || [ $var == "CPR" ]; then
			cdo timselsum,6 ${exp}_${var}_${season}_${year_end}_84days.nc ${exp}_${var}_${season}_${year_end}_14t.nc
			else
			cdo timselmean,6 ${exp}_${var}_${season}_${year_end}_84days.nc ${exp}_${var}_${season}_${year_end}_14t.nc
			fi
		    done #loop over variable
done #ensemble

#calculate Ensemble mean
cdo ensmean ${runexp}a.monthly_TS.${season}_${year_end}.nc ${runexp}b.monthly_TS.${season}_${year_end}.nc \
    ${runexp}c.monthly_TS.${season}_${year_end}.nc ${runexp}d.monthly_TS.${season}_${year_end}.nc \
    ${runexp}e.monthly_TS.${season}_${year_end}.nc ${runexp}f.monthly_TS.${season}_${year_end}.nc \
    ${runexp}g.monthly_TS.${season}_${year_end}.nc ${runexp}h.monthly_TS.${season}_${year_end}.nc \
    ${runexp}i.monthly_TS.${season}_${year_end}.nc ${runexp}j.monthly_TS.${season}_${year_end}.nc \
    ${runexp}k.monthly_TS.${season}_${year_end}.nc ${runexp}l.monthly_TS.${season}_${year_end}.nc \
    ${runexp}m.monthly_TS.${season}_${year_end}.nc ${runexp}n.monthly_TS.${season}_${year_end}.nc \
    ${runexp}o.monthly_TS.${season}_${year_end}.nc ${runexp}p.monthly_TS.${season}_${year_end}.nc \
	${runexp}_ensmean_monthly_${season}_${year_end}.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"ACCESS1.3b model output from GLACE-1 experiment\n" ${runexp}_ensmean_monthly_${season}_${year_end}.nc
ncatted -h -O -a institution,global,a,c,"ARC Centre of Excellence for Climate System Science, model run at NCI National Facility\n" ${runexp}_ensmean_monthly_${season}_${year_end}.nc
ncatted -h -O -a source,global,a,c,"ACCESS1.3b (HadGEM3-CABLE2.0), AMIP, N96\n" ${runexp}_ensmean_monthly_${season}_${year_end}.nc
ncatted -h -O -a  contact,global,a,c,"r.lorenz@unsw.edu.au \n" ${runexp}_ensmean_monthly_${season}_${year_end}.nc

cdo ensmean ${runexp}a.daily.${season}_${year_end}.nc ${runexp}b.daily.${season}_${year_end}.nc \
    ${runexp}c.daily.${season}_${year_end}.nc ${runexp}d.daily.${season}_${year_end}.nc \
    ${runexp}e.daily.${season}_${year_end}.nc ${runexp}f.daily.${season}_${year_end}.nc \
    ${runexp}g.daily.${season}_${year_end}.nc ${runexp}h.daily.${season}_${year_end}.nc \
    ${runexp}i.daily.${season}_${year_end}.nc ${runexp}j.daily.${season}_${year_end}.nc \
    ${runexp}k.daily.${season}_${year_end}.nc ${runexp}l.daily.${season}_${year_end}.nc \
    ${runexp}m.daily.${season}_${year_end}.nc ${runexp}n.daily.${season}_${year_end}.nc \
    ${runexp}o.daily.${season}_${year_end}.nc ${runexp}p.daily.${season}_${year_end}.nc \
	${runexp}_ensmean_daily_${season}_${year_end}.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"ACCESS1.3b model output from GLACE-1 experiment\n" ${runexp}_ensmean_daily_${season}_${year_end}.nc
ncatted -h -O -a institution,global,a,c,"ARC Centre of Excellence for Climate System Science, model run at NCI National Facility\n" ${runexp}_ensmean_daily_${season}_${year_end}.nc
ncatted -h -O -a source,global,a,c,"ACCESS1.3b (HadGEM3-CABLE2.0), AMIP, N96\n" ${runexp}_ensmean_daily_${season}_${year_end}.nc
ncatted -h -O -a  contact,global,a,c,"r.lorenz@unsw.edu.au \n" ${runexp}_ensmean_daily_${season}_${year_end}.nc

for var in T TN TX PR CPR
do
    cdo ensmean ${runexp}a_${var}_${season}_${year_end}_14t.nc ${runexp}b_${var}_${season}_${year_end}_14t.nc \
	${runexp}c_${var}_${season}_${year_end}_14t.nc ${runexp}d_${var}_${season}_${year_end}_14t.nc \
	${runexp}e_${var}_${season}_${year_end}_14t.nc ${runexp}f_${var}_${season}_${year_end}_14t.nc \
	${runexp}g_${var}_${season}_${year_end}_14t.nc ${runexp}h_${var}_${season}_${year_end}_14t.nc \
	${runexp}i_${var}_${season}_${year_end}_14t.nc ${runexp}j_${var}_${season}_${year_end}_14t.nc \
	${runexp}k_${var}_${season}_${year_end}_14t.nc ${runexp}l_${var}_${season}_${year_end}_14t.nc \
	${runexp}m_${var}_${season}_${year_end}_14t.nc ${runexp}n_${var}_${season}_${year_end}_14t.nc \
	${runexp}o_${var}_${season}_${year_end}_14t.nc ${runexp}p_${var}_${season}_${year_end}_14t.nc \
	${runexp}_ensmean_${var}_${season}_${year_end}.nc

#calculate full ensemble variance
    ncecat ${runexp}?_${var}_${season}_${year_end}_14t.nc ${runexp}_full_ens_${var}_${season}_${year_end}.nc

#add global attributes
ncatted -h -O -a title,global,a,c,"ACCESS1.3b model output from GLACE-1 experiment\n" ${runexp}_ensmean_${var}_${season}_${year_end}.nc
ncatted -h -O -a institution,global,a,c,"ARC Centre of Excellence for Climate System Science, model run at NCI National Facility\n" ${runexp}_ensmean_${var}_${season}_${year_end}.nc
ncatted -h -O -a source,global,a,c,"ACCESS1.3b (HadGEM3-CABLE2.0), AMIP, N96\n" ${runexp}_ensmean_${var}_${season}_${year_end}.nc
ncatted -h -O -a  contact,global,a,c,"r.lorenz@unsw.edu.au \n" ${runexp}_ensmean_${var}_${season}_${year_end}.nc

ncatted -h -O -a title,global,a,c,"ACCESS1.3b model output from GLACE-1 experiment\n" ${runexp}_full_ens_${var}_${season}_${year_end}.nc
ncatted -h -O -a institution,global,a,c,"ARC Centre of Excellence for Climate System Science, model run at NCI National Facility\n" ${runexp}_full_ens_${var}_${season}_${year_end}.nc
ncatted -h -O -a source,global,a,c,"ACCESS1.3b (HadGEM3-CABLE2.0), AMIP, N96\n" ${runexp}_full_ens_${var}_${season}_${year_end}.nc
ncatted -h -O -a  contact,global,a,c,"r.lorenz@unsw.edu.au \n" ${runexp}_full_ens_${var}_${season}_${year_end}.nc

if [[ ${var} == "T" ]]; then
ncatted -h -O -a  comment,global,a,c,"Ensemble mean for GLACE-1, 6 day mean for temperature" ${runexp}_ensmean_${var}_${season}_${year_end}.nc
ncatted -h -O -a  comment,global,a,c,"Full ensemble for GLACE-1, 6 day mean for temperature" ${runexp}_full_ens_${var}_${season}_${year_end}.nc
elif [[ ${var} == "TN" ]]; then
ncatted -h -O -a  comment,global,a,c,"Ensemble mean for GLACE-1, 6 day mean for minimum temperature" ${runexp}_ensmean_${var}_${season}_${year_end}.nc
ncatted -h -O -a  comment,global,a,c,"Full ensemble for GLACE-1, 6 day mean for minimum temperature" ${runexp}_full_ens_${var}_${season}_${year_end}.nc
elif [[ ${var} == "TX" ]]; then
ncatted -h -O -a  comment,global,a,c,"Ensemble mean for GLACE-1, 6 day mean for maximum temperature" ${runexp}_ensmean_${var}_${season}_${year_end}.nc
ncatted -h -O -a  comment,global,a,c,"Full ensemble for GLACE-1, 6 day mean for maximum temperature" ${runexp}_full_ens_${var}_${season}_${year_end}.nc
elif [[ ${var} == "PR" ]]; then
ncatted -h -O -a  comment,global,a,c,"Ensemble mean for GLACE-1, 6 day sum for precipitation" ${runexp}_ensmean_${var}_${season}_${year_end}.nc
ncatted -h -O -a  comment,global,a,c,"Full ensemble for GLACE-1, 6 day sum for precipitation" ${runexp}_full_ens_${var}_${season}_${year_end}.nc
fi

# Move output data to OUTDIR space.
    echo "The variance output files for ${var} are now moved to ${outdir}."
    mv ${runexp}_ensmean_${var}_${season}_${year_end}.nc ${outdir}/
    mv ${runexp}_full_ens_${var}_${season}_${year_end}.nc ${outdir}/
done #2nd loop over variables

echo "The monthly output files are now moved to ${outdir}."
mv ?????.monthly_TS.${season}_${year_end}.nc ${outdir}/
mv ${runexp}_ensmean_monthly_${season}_${year_end}.nc ${outdir}/

echo "The daily output files are now moved to ${outdir}."
mv ?????.daily.${season}_${year_end}.nc ${outdir}/
mv ${runexp}_ensmean_daily_${season}_${year_end}.nc ${outdir}/

#clean up
rm ${workdir}/*

#END
