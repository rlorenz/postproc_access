#!/bin/bash

#PBS -q normal
#PBS -N test_leap
#PBS -j oe
#PBS -l ncpus=1
#PBS -l vmem=1536mb
#PBS -l walltime=00:00:02
#PBS -wd

#function to determine if year is leapyear (1) or not (0)

function leapyr()
{ 
  if [[  ${year}%4 -eq "0" && ${year}%100 -ne "0" || ${year}%400 -eq "0" ]]; then
    local __lpyr=1
  else
    local __lpyr=0
  fi
  echo "$__lpyr"
}

year=1982

leapyear=$(leapyr)
let endT=335+$leapyear

echo "Leapyear: $leapyear , endT: $endT"

