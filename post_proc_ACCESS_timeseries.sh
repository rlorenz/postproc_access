#!/bin/bash
## \file post_proc_ACCESS_timeseries.sh
#  \author Ruth Lorenz (r.lorenz@unsw.edu.au)
#  \brief create timeseries for UM output which is archived on mdss as monthly *.nc.gz files
#  \usage qsub -V -v -lother=mdss post_proc_ACCESS_timeseries.sh

#PBS -q copyq
#PBS -N postproc
#PBS -P dt6
#PBS -j oe
#PBS -l other=mdss
#PBS -l ncpus=1
#PBS -l mem=6000mb
#PBS -l walltime=10:00:00
#PBS -l wd

##-----------------------
# function to determine if year is leapyear (1) or not (0)
##-----------------------

function leapyr ()
{ 
  if [[  ${year}%4 -eq "0" && ${year}%100 -ne "0" || ${year}%400 -eq "0" ]]; then
    local __lpyr=1
  else
    local __lpyr=0
  fi
  echo "$__lpyr"
}

##-----------------------##
## load required modules ##
##-----------------------##

module load netcdf
module load nco
module load cdo
 
##---------------------##
## user specifications ##
##-------------------- ##

exp=vacdl
year_start=1978
year_end=2011
archive=$USER/$exp/
workdir=/short/dt6/$USER/postproc/$exp/work/
outdir=/short/dt6/$USER/postproc/$exp/timeseries/

#infos for global attributes in netcdf
TITLE_RUN="ACCESS1.3b output from Land use change experiments,\n 025GPwc E3"
SOURCE="ACCESS1.3b (HadGEM3-CABLE2.0), AMIP, N96\n"
INSTITUTION="ARC Centre of Excellence for Climate System Science,\n model run at NCI National Facility, Australia\n"
CONTACT="r.lorenz@unsw.edu.au"

##---------------------##

mkdir -p /short/dt6/$USER/postproc/$exp
mkdir -p ${outdir}
mkdir -p ${outdir}/yearly
mkdir -p ${workdir}

cd ${workdir}

##-------------------------------------------##
## monthly files:                            ##
##-------------------------------------------##

## get data from mdss, loop over year, unzip
## and concatenate yearly archives          

year=$year_start

     while [  $year -le $year_end ]; do
       #loop over years
       echo "Processing year $year"

            #for first year get december of year before (for seasonal means)
	    if [ $year -eq $year_start ]; then
		 let "year_before=year-1"
		 #check if file available on mdss
		 file=`mdss dmls -l ${archive}/$exp.monthly.$year_before-12-00T00.nc.gz | awk '{print $2}'`
		 if [[ "$file" != 1 ]]; then
		     echo "ERROR: missing file $exp.monthly.$year_before-12-00T00.nc.gz  in ${archive}" 1>&2
		     exit 1
		 fi		    
		 mdss get ${archive}/$exp.monthly.$year_before-12-00T00.nc.gz
		 gunzip $exp.monthly.$year_before-12-00T00.nc.gz

	     fi

		for month in 01 02 03 04 05 06 07 08 09 10 11 12
		  do
		  #monthly files, check availability first
                  file_mon=`mdss dmls -l ${archive}/$exp.monthly.$year-$month-00T00.nc.gz | awk '{print $2}'`
                  if [[ "$file_mon" != 1 ]]; then
        	   echo "ERROR: missing file $exp.monthly.$year-$month-00T00.nc.gz in ${archive}" 1>&2
         	   exit 1
                  fi
		  mdss get ${archive}/$exp.monthly.$year-$month-00T00.nc.gz
		  gunzip $exp.monthly.$year-$month-00T00.nc.gz

		  done
		
		  if [ $year -eq $year_start ]; then
		      let "year_before=year-1"
		      ncrcat -O $exp.monthly.$year_before-12-00T00.nc $exp.monthly.$year-??-00T00.nc \
			  ${workdir}/$exp.monthly.dec$year_before-$year.nc
		      ncrcat -O $exp.monthly.$year-??-00T00.nc ${workdir}/$exp.monthly.$year.nc
		  elif [ $year -eq $year_end ]; then
		      ncrcat -O $exp.monthly.$year-??-00T00.nc ${workdir}/$exp.monthly.$year.nc
		      if [ $year_end -ne 2012]; then
		      rm $exp.monthly.$year-12-00T00.nc
		      fi
		      ncrcat -O $exp.monthly.$year-??-00T00.nc ${workdir}/$exp.monthly.$year-dec.nc
		  else 
		      ncrcat -O $exp.monthly.$year-??-00T00.nc ${workdir}/$exp.monthly.$year.nc
		  fi

                  # clean up
                  rm $exp.monthly.????-??-00T00.nc

		let year=year+1
	done #year

#Create timeseries over whole time frame
# monthly mean climatology and seasonal means for monthly files

echo "Create seasonal averages and monthly timeseries..."

#create timeseries and monthly means
ncrcat -O $exp.monthly.????.nc $exp.monthly_TS.${year_start}_${year_end}.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" $exp.monthly_TS.${year_start}_${year_end}.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" $exp.monthly_TS.${year_start}_${year_end}.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" $exp.monthly_TS.${year_start}_${year_end}.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" $exp.monthly_TS.${year_start}_${year_end}.nc
ncatted -h -O -a  comment,global,a,c,"Monthly timeseries" $exp.monthly_TS.${year_start}_${year_end}.nc

ncea -O $exp.monthly.????.nc $exp.monthly_MSC.${year_start}_${year_end}.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" $exp.monthly_MSC.${year_start}_${year_end}.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" $exp.monthly_MSC.${year_start}_${year_end}.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" $exp.monthly_MSC.${year_start}_${year_end}.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" $exp.monthly_MSC.${year_start}_${year_end}.nc
ncatted -h -O -a  comment,global,a,c,"Monthly climatology" $exp.monthly_MSC.${year_start}_${year_end}.nc

#copy yearly files to outdir/yearly for future calculation of other timeperiods
cp $exp.monthly.????.nc ${outdir}/yearly/

#create seasonal averages
rm $exp.monthly.$year_end.nc
rm $exp.monthly.$year_start.nc
ncrcat -O $exp.monthly.dec$year_before-$year_start.nc $exp.monthly.????.nc $exp.monthly.$year_end-dec.nc \
    $exp.monthly_TSseas.${year_start}_${year_end}.nc

cdo yseasmean $exp.monthly_TSseas.${year_start}_${year_end}.nc $exp.seasonal_means.${year_start}_${year_end}.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" $exp.seasonal_means.${year_start}_${year_end}.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" $exp.seasonal_means.${year_start}_${year_end}.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" $exp.seasonal_means.${year_start}_${year_end}.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" $exp.seasonal_means.${year_start}_${year_end}.nc
ncatted -h -O -a  comment,global,a,c,"Seasonal means over all years" $exp.seasonal_means.${year_start}_${year_end}.nc
# clean up
#rm $exp.monthly.????.nc
rm $exp.monthly.dec$year_before-$year_start.nc
rm $exp.monthly.$year_end-dec.nc
rm $exp.monthly_TSseas.${year_start}_${year_end}.nc

#create yearly means
cdo yearavg $exp.monthly_TS.${year_start}_${year_end}.nc $exp.yearly_means.${year_start}_${year_end}.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" $exp.yearly_means.${year_start}_${year_end}.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" $exp.yearly_means.${year_start}_${year_end}.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" $exp.yearly_means.${year_start}_${year_end}.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" $exp.yearly_means.${year_start}_${year_end}.nc
ncatted -h -O -a  comment,global,a,c,"Yearly means" $exp.yearly_means.${year_start}_${year_end}.nc

## zip and move monthly timeseries to output

gzip $exp.monthly_TS.${year_start}_${year_end}.nc
gzip $exp.monthly_MSC.${year_start}_${year_end}.nc
gzip $exp.seasonal_means.${year_start}_${year_end}.nc
gzip $exp.yearly_means.${year_start}_${year_end}.nc

# Move output data to OUTDIR space.
echo "The monthly output files are now moved to ${outdir}."

mv $exp.monthly_TS.${year_start}_${year_end}.nc.gz ${outdir}
mv $exp.monthly_MSC.${year_start}_${year_end}.nc.gz ${outdir}
mv $exp.seasonal_means.${year_start}_${year_end}.nc.gz ${outdir}
mv $exp.yearly_means.${year_start}_${year_end}.nc.gz ${outdir}

##-------------------------------------------##
## daily, 3 hourly, and hourly files:        ##
##-------------------------------------------##

#for timeperiod in daily 3hourly hourly
for timeperiod in daily
  do

  year=$year_start
#  if  [ $year_end -eq 2012]; then
#      let year_end=$year_end-1
#  fi

     while [  $year -le $year_end ]; do
       #loop over years
       echo "Processing year $year for timeperiod $timeperiod"

		for month in 01 02 03 04 05 06 07 08 09 10 11 12
		  do
		  file=`mdss dmls -l ${archive}/$exp.$timeperiod.$year-$month-00T00.nc.gz | awk '{print $2}'`
		  if [[ "$file" != 1 ]]; then
		      echo "ERROR: missing file $exp.$timeperiod.$year-$month-00T00.nc.gz in ${archive}" 1>&2
		      exit 2
		  fi
		  mdss get ${archive}/$exp.$timeperiod.$year-$month-00T00.nc.gz
		  gunzip $exp.$timeperiod.$year-$month-00T00.nc.gz
		  
		  done #month

		  ncrcat -O $exp.$timeperiod.$year-??-00T00.nc $exp.$timeperiod.$year.nc
		  rm $exp.$timeperiod.$year-??-00T00.nc

		  #if daily, extract tmax and tmin for monthly means
		  if [[ "$timeperiod" == "daily" ]]; then
		      ncks -O -v tasmax,tasmin $exp.$timeperiod.$year.nc $exp.${timeperiod}_tmax_tmin.$year.nc
		      
		      #take december from year before but not last december for seasonal means
		      if [ $year -eq $year_start ]; then
		      let year_before=year-1
		      mdss get ${archive}/$exp.$timeperiod.${year_before}-12-00T00.nc.gz
		      gunzip $exp.$timeperiod.${year_before}-12-00T00.nc.gz
		      ncks -O -v tasmax,tasmin $exp.$timeperiod.${year_before}-12-00T00.nc $exp.${timeperiod}_tmax_tmin.dec$year_before.nc
		      ncrcat -O $exp.${timeperiod}_tmax_tmin.dec$year_before.nc $exp.${timeperiod}_tmax_tmin.$year.nc $exp.${timeperiod}_tmax_tmin.dec${year_before}_$year.nc
		      cdo monmean $exp.${timeperiod}_tmax_tmin.dec${year_before}_$year.nc $exp.monmean_tmax_tmin.dec${year_before}_$year.nc
		      elif [ $year -eq $year_end ]; then
			  let endT=333+$(leapyr)
			  echo "$year is leapyear?: $(leapyr)"
		      ncks -O -d time,0,$endT $exp.${timeperiod}_tmax_tmin.$year_end.nc $exp.${timeperiod}_tmax_tmin.$year_end-dec.nc
		      cdo monmean $exp.${timeperiod}_tmax_tmin.$year_end-dec.nc $exp.monmean_tmax_tmin.$year_end-dec.nc
		      fi
		      cdo monmean $exp.${timeperiod}_tmax_tmin.$year.nc $exp.monmean_tmax_tmin.$year.nc

		  fi

		  #if hourly -> separate variables
		  if [[ "$timeperiod" == "hourly" ]]; then
		      ncks -O -x -v hfls,tas,hfss $exp.$timeperiod.$year.nc $exp.${timeperiod}_pr.$year.nc
		      ncks -O -x -v pr,tas,hfss $exp.$timeperiod.$year.nc $exp.${timeperiod}_hfls.$year.nc
		      ncks -O -x -v pr,tas,hfls $exp.$timeperiod.$year.nc $exp.${timeperiod}_hfss.$year.nc
		      ncks -O -x -v pr,hfls,hfss $exp.$timeperiod.$year.nc $exp.${timeperiod}_tas.$year.nc
		  fi

		  let year=year+1
     done #year

  # Create timeseries
  echo "Create $timeperiod timeseries..."
  if [[ "$timeperiod" == "hourly" ]]; then
  ncrcat -O $exp.${timeperiod}_pr.????.nc $exp.${timeperiod}_pr.${year_start}_${year_end}.nc
  #add global attributes
  ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" $exp.${timeperiod}_pr.${year_start}_${year_end}.nc
  ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" $exp.${timeperiod}_pr.${year_start}_${year_end}.nc
  ncatted -h -O -a source,global,a,c,"${SOURCE}" $exp.${timeperiod}_pr.${year_start}_${year_end}.nc
  ncatted -h -O -a  contact,global,a,c,"${CONTACT}" $exp.${timeperiod}_pr.${year_start}_${year_end}.nc

  ncrcat -O $exp.${timeperiod}_hfls.????.nc $exp.${timeperiod}_hfls.${year_start}_${year_end}.nc
  #add global attributes
  ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" $exp.${timeperiod}_hfls.${year_start}_${year_end}.nc
  ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" $exp.${timeperiod}_hfls.${year_start}_${year_end}.nc
  ncatted -h -O -a source,global,a,c,"${SOURCE}" $exp.${timeperiod}_hfls.${year_start}_${year_end}.nc
  ncatted -h -O -a  contact,global,a,c,"${CONTACT}" $exp.${timeperiod}_hfls.${year_start}_${year_end}.nc

  ncrcat -O $exp.${timeperiod}_hfss.????.nc $exp.${timeperiod}_hfss.${year_start}_${year_end}.nc
 #add global attributes
 ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" $exp.${timeperiod}_hfss.${year_start}_${year_end}.nc
 ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" $exp.${timeperiod}_hfss.${year_start}_${year_end}.nc
 ncatted -h -O -a source,global,a,c,"${SOURCE}" $exp.${timeperiod}_hfss.${year_start}_${year_end}.nc
 ncatted -h -O -a  contact,global,a,c,"${CONTACT}" $exp.${timeperiod}_hfss.${year_start}_${year_end}.nc

  ncrcat -O $exp.${timeperiod}_tas.????.nc $exp.${timeperiod}_tas.${year_start}_${year_end}.nc
  #add global attributes
  ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" $exp.${timeperiod}_tas.${year_start}_${year_end}.nc
  ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" $exp.${timeperiod}_tas.${year_start}_${year_end}.nc
  ncatted -h -O -a source,global,a,c,"${SOURCE}" $exp.${timeperiod}_tas.${year_start}_${year_end}.nc
  ncatted -h -O -a  contact,global,a,c,"${CONTACT}" $exp.${timeperiod}_tas.${year_start}_${year_end}.nc

  elif [[ "$timeperiod" == "daily" ]]; then
  ncrcat -O $exp.monmean_tmax_tmin.????.nc $exp.monmean_tmax_tmin.${year_start}_${year_end}.nc
  #add global attributes
  ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" $exp.monmean_tmax_tmin.${year_start}_${year_end}.nc
  ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" $exp.monmean_tmax_tmin.${year_start}_${year_end}.nc
  ncatted -h -O -a source,global,a,c,"${SOURCE}" $exp.monmean_tmax_tmin.${year_start}_${year_end}.nc
  ncatted -h -O -a  contact,global,a,c,"${CONTACT}" $exp.monmean_tmax_tmin.${year_start}_${year_end}.nc

  rm $exp.monmean_tmax_tmin.$year_start.nc
  rm $exp.monmean_tmax_tmin.$year_end.nc
  ncrcat -O $exp.monmean_tmax_tmin.dec${year_before}_????.nc $exp.monmean_tmax_tmin.????.nc $exp.monmean_tmax_tmin.$year_end-dec.nc $exp.monmean_tmax_tmin_forseas.${year_start}_${year_end}.nc
  cdo yseasmean $exp.monmean_tmax_tmin_forseas.${year_start}_${year_end}.nc $exp.seasmean_tmax_tmin.${year_start}_${year_end}.nc
  #add global attributes
  ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" $exp.seasmean_tmax_tmin.${year_start}_${year_end}.nc
  ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" $exp.seasmean_tmax_tmin.${year_start}_${year_end}.nc
  ncatted -h -O -a source,global,a,c,"${SOURCE}" $exp.seasmean_tmax_tmin.${year_start}_${year_end}.nc
  ncatted -h -O -a  contact,global,a,c,"${CONTACT}" $exp.seasmean_tmax_tmin.${year_start}_${year_end}.nc

  ncrcat -O $exp.${timeperiod}.????.nc $exp.${timeperiod}_TS.${year_start}_${year_end}.nc
  #add global attributes
  ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" $exp.${timeperiod}_TS.${year_start}_${year_end}.nc
  ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" $exp.${timeperiod}_TS.${year_start}_${year_end}.nc
  ncatted -h -O -a source,global,a,c,"${SOURCE}" $exp.${timeperiod}_TS.${year_start}_${year_end}.nc
  ncatted -h -O -a  contact,global,a,c,"${CONTACT}" $exp.${timeperiod}_TS.${year_start}_${year_end}.nc

  else
  ncrcat -O $exp.${timeperiod}.????.nc $exp.${timeperiod}_TS.${year_start}_${year_end}.nc
  #add global attributes
  ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" $exp.${timeperiod}_TS.${year_start}_${year_end}.nc
  ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" $exp.${timeperiod}_TS.${year_start}_${year_end}.nc
  ncatted -h -O -a source,global,a,c,"${SOURCE}" $exp.${timeperiod}_TS.${year_start}_${year_end}.nc
  ncatted -h -O -a  contact,global,a,c,"${CONTACT}" $exp.${timeperiod}_TS.${year_start}_${year_end}.nc

  fi

  #clean up 
  #rm $exp.$timeperiod.????.nc
  mv $exp.$timeperiod.????.nc ${outdir}/yearly/

  #prepare daily Tmax,Tmin and PR for climdex
  if  [[ "$timeperiod" == "daily" ]]; then
      ncks -O -v tasmin ${exp}.daily_TS.${year_start}_${year_end}.nc ${exp}_TN_${year_start}-${year_end}.nc
      ncrename -v tasmin,Tmin ${exp}_TN_${year_start}-${year_end}.nc
      cdo setreftime,1950-01-01,00:00:00,days ${exp}_TN_${year_start}-${year_end}.nc ${exp}_TN_${year_start}-${year_end}_ref1950.nc
      ncatted -a missing_value,Tmin,d,, ${exp}_TN_${year_start}-${year_end}_ref1950.nc
      ncatted -a _FillValue,Tmin,d,, ${exp}_TN_${year_start}-${year_end}_ref1950.nc
      cdo subc,273.15 ${exp}_TN_${year_start}-${year_end}_ref1950.nc ${exp}_TN_${year_start}-${year_end}_degC.nc
      rm ${exp}_TN_${year_start}-${year_end}_ref1950.nc

      ncks -O -v tasmax ${exp}.daily_TS.${year_start}_${year_end}.nc ${exp}_TX_${year_start}-${year_end}.nc
      ncrename -v tasmax,Tmax ${exp}_TX_${year_start}-${year_end}.nc
      cdo setreftime,1950-01-01,00:00:00,days ${exp}_TX_${year_start}-${year_end}.nc ${exp}_TX_${year_start}-${year_end}_ref1950.nc
      ncatted -a missing_value,Tmax,d,, ${exp}_TX_${year_start}-${year_end}_ref1950.nc
      ncatted -a _FillValue,Tmax,d,, ${exp}_TX_${year_start}-${year_end}_ref1950.nc
      cdo subc,273.15 ${exp}_TX_${year_start}-${year_end}_ref1950.nc ${exp}_TX_${year_start}-${year_end}_degC.nc
      rm ${exp}_TX_${year_start}-${year_end}_ref1950.nc

      ncks -O -v pr ${exp}.daily_TS.${year_start}_${year_end}.nc ${exp}_PR_${year_start}-${year_end}.nc
      ncrename -v pr,prcp ${exp}_PR_${year_start}-${year_end}.nc
      cdo setreftime,1950-01-01,00:00:00,days ${exp}_PR_${year_start}-${year_end}.nc ${exp}_PR_${year_start}-${year_end}_ref1950.nc
      ncatted -a missing_value,prcp,d,, ${exp}_PR_${year_start}-${year_end}_ref1950.nc
      ncatted -a _FillValue,prcp,d,, ${exp}_PR_${year_start}-${year_end}_ref1950.nc
      cdo mulc,86400 ${exp}_PR_${year_start}-${year_end}_ref1950.nc  ${exp}_PR_${year_start}-${year_end}_mm.nc
      rm ${exp}_PR_${year_start}-${year_end}_ref1950.nc

  fi

  #compress file
  if [[ "$timeperiod" == "hourly" ]]; then
      gzip $exp.${timeperiod}_pr.${year_start}_${year_end}.nc  
      gzip $exp.${timeperiod}_hfls.${year_start}_${year_end}.nc
      gzip $exp.${timeperiod}_tas.${year_start}_${year_end}.nc
      gzip $exp.${timeperiod}_hfss.${year_start}_${year_end}.nc
  elif [[ "$timeperiod" == "daily" ]]; then
      gzip $exp.seasmean_tmax_tmin.${year_start}_${year_end}.nc
      gzip $exp.monmean_tmax_tmin.${year_start}_${year_end}.nc
      gzip $exp.${timeperiod}_TS.${year_start}_${year_end}.nc
  else
      gzip $exp.${timeperiod}_TS.${year_start}_${year_end}.nc
  fi

  # Move output data to output DIR.
  echo "The $timeperiod output files are now moved to ${outdir}."

  if [[ "$timeperiod" == "hourly" ]]; then
  mv $exp.${timeperiod}_pr.${year_start}_${year_end}.nc.gz ${outdir}
  mv $exp.${timeperiod}_hfls.${year_start}_${year_end}.nc.gz ${outdir}
  mv $exp.${timeperiod}_hfss.${year_start}_${year_end}.nc.gz ${outdir}
  mv $exp.${timeperiod}_tas.${year_start}_${year_end}.nc.gz ${outdir}
  elif [[ "$timeperiod" == "daily" ]]; then
  mv ${exp}_TN_${year_start}-${year_end}_degC.nc ${outdir}
  mv ${exp}_TX_${year_start}-${year_end}_degC.nc ${outdir}
  mv ${exp}_PR_${year_start}-${year_end}_mm.nc ${outdir}
  mv $exp.monmean_tmax_tmin.${year_start}_${year_end}.nc.gz ${outdir}
  mv $exp.seasmean_tmax_tmin.${year_start}_${year_end}.nc.gz ${outdir}
  mv $exp.${timeperiod}_TS.${year_start}_${year_end}.nc.gz ${outdir}
  else
  mv $exp.${timeperiod}_TS.${year_start}_${year_end}.nc.gz ${outdir}
  fi

done #timeperiod

#clean up
rm ${workdir}/*

#END
