#!/bin/bash
## \file LUC_exp_T_PR_for_publish.sh
#  \author Ruth Lorenz (r.lorenz@unsw.edu.au)
#  \brief cut T and PR variables from LUC experiments for publishing
#  \usage qsub LUC_exp_T_PR_for_publish.sh

#PBS -q copyq
#PBS -N ncks_t_p_luc
#PBS -P dt6
#PBS -j oe
#PBS -l other=mdss
#PBS -l ncpus=1
#PBS -l mem=6000mb
#PBS -l walltime=5:00:00
#PBS -l wd

##-----------------------##
## load required modules ##
##-----------------------##

module load netcdf
module load nco

##---------------------##
## user specifications ##
##-------------------- ##

year_start=1978
year_end=2011
outdir=/short/w35/$USER/postproc/LUC/V2/

mkdir -p ${outdir}

##-------------------- ##
## loop over LUC experiments

#for exp in uaoya uaoyb uaoyc uaoyd uaoye uaoyf uaoyg uaoyh uaoyi uaoyj uaoyk uaoyl uaoym uaoyn uaoyo uaoyp uaoyr #V1
for exp in uaoys uaoyt uaoyw uaoyx uaoyy uaoyz #Apr 2014
# vacda vacdb vacdc vacdd vacde vacdf vacdg vacdh vacdi vacdj vacdk vacdl #Aug 2014
#vacdm vacdn vacdo vacdp vacdq vacdr vacds vacdt vacdu vacdv #Nov 2014
#vajoa vajob vajoc vajod vajoe vajof vajog vajoh vajoi vajoj vajok vajol vajom vajon vajoo vajop vajoq vajor vajos vajot vajou vajov vacdw vacdx vacdy vacdz #Mar 2015, V2. additional ensemble members
do

archive=/short/dt6/$USER/postproc/$exp/
workdir=/short/dt6/$USER/postproc/$exp/work/

cd ${workdir}

cp $archive/timeseries/${exp}.monthly_TS.${year_start}_${year_end}.nc.gz ${workdir}
gunzip ${exp}.monthly_TS.${year_start}_${year_end}.nc.gz

ncatted -h -O -a history,global,a,c,"\nApr 2014 ncrcat -O "$exp".monthly.????.nc "$exp".monthly_TS."${year_start}"_"${year_end}".nc\n" ${exp}.monthly_TS.${year_start}_${year_end}.nc
ncatted -h -O -a history,global,a,c,"Apr 2014 ncrcat -O "$exp".monthly.YEAR-??-00T00.nc "$exp".monthly.YEAR.nc\n" ${exp}.monthly_TS.${year_start}_${year_end}.nc

ncks -v tas,pr ${exp}.monthly_TS.${year_start}_${year_end}.nc ${exp}.tas_pr_monthly_TS.${year_start}_${year_end}.nc

ncatted -h -O -a title,global,a,c,"ACCESS1.3b model output \n from Land use change experiments \n gradual increasing area of forest pft changed to grass \n" ${exp}.tas_pr_monthly_TS.${year_start}_${year_end}.nc

ncatted -h -O -a institution,global,a,c,"ARC Centre of Excellence for Climate System Science, model run at NCI National Facility\n" ${exp}.tas_pr_monthly_TS.${year_start}_${year_end}.nc

ncatted -h -O -a source,global,a,c,"ACCESS1.3b (HadGEM3-CABLE2.0), AMIP, N96\n" ${exp}.tas_pr_monthly_TS.${year_start}_${year_end}.nc

ncatted -h -O -a  contact,global,a,c,"r.lorenz@unsw.edu.au \n" ${exp}.tas_pr_monthly_TS.${year_start}_${year_end}.nc

#if [[ "$exp" == "uaoyr" ]]; then
#    runname=CTL
#elif [[ "$exp" == "uaoya" ]]; then
#    runname=121GPsc
#elif [[ "$exp" == "uaoyb" ]]; then
#    runname=121GPwc
#elif [[ "$exp" == "uaoyc" ]]; then
#    runname=001GPsc
#elif [[ "$exp" == "uaoyd" ]]; then
#    runname=001GPwc
#elif [[ "$exp" == "uaoye" ]]; then
#    runname=003GPsc
#elif [[ "$exp" == "uaoyf" ]]; then
#    runname=003GPwc
#elif [[ "$exp" == "uaoyg" ]]; then
#    runname=005GPsc
#elif [[ "$exp" == "uaoyh" ]]; then
#    runname=005GPwc
#elif [[ "$exp" == "uaoyi" ]]; then
#    runname=009GPsc
#elif [[ "$exp" == "uaoyj" ]]; then
#    runname=009GPwc
#elif [[ "$exp" == "uaoyk" ]]; then
#    runname=025GPsc
#elif [[ "$exp" == "uaoyl" ]]; then
#    runname=025GPwc
#elif [[ "$exp" == "uaoym" ]]; then
#    runname=049GPsc
#elif [[ "$exp" == "uaoyn" ]]; then
#    runname=049GPwc
#elif [[ "$exp" == "uaoyo" ]]; then
#    runname=081GPsc
#elif [[ "$exp" == "uaoyp" ]]; then
#    runname=081GPwc
#fi
if [[ "$exp" == "uaoys" ]]; then
    runname=CTL_E1
elif [[ "$exp" == "uaoyt" ]]; then
    runname=CTL_E2
elif [[ "$exp" == "vajoa" ]]; then
    runname=CTL_E3
elif [[ "$exp" == "vajob" ]]; then
    runname=CTL_E4
fi

if [[ "$exp" == "vacdc" ]]; then
    runname=001GPwc_E1
elif [[ "$exp" == "vacdd" ]]; then
    runname=001GPwc_E2
elif [[ "$exp" == "vajoc" ]]; then
    runname=001GPwc_E3
elif [[ "$exp" == "vajod" ]]; then
    runname=001GPwc_E4
fi

if [[ "$exp" == "vacda" ]]; then
    runname=001GPsc_E1
elif [[ "$exp" == "vacdb" ]]; then
    runname=001GPsc_E2
elif [[ "$exp" == "vajoe" ]]; then
    runname=001GPsc_E3
elif [[ "$exp" == "vajof" ]]; then
    runname=001GPsc_E4
fi

if [[ "$exp" == "vacdg" ]]; then
    runname=009GPwc_E1
elif [[ "$exp" == "vacdh" ]]; then
    runname=009GPwc_E2
elif [[ "$exp" == "vajog" ]]; then
    runname=009GPwc_E3
elif [[ "$exp" == "vajoh" ]]; then
    runname=009GPwc_E4
fi

if [[ "$exp" == "vacde" ]]; then
    runname=009GPsc_E1
elif [[ "$exp" == "vacdf" ]]; then
    runname=009GPsc_E2
elif [[ "$exp" == "vajoi" ]]; then
    runname=009GPsc_E3
elif [[ "$exp" == "vajoj" ]]; then
    runname=009GPsc_E4
fi

if [[ "$exp" == "vacdk" ]]; then
    runname=025GPwc_E1
elif [[ "$exp" == "vacdl" ]]; then
    runname=025GPwc_E2
elif [[ "$exp" == "vajok" ]]; then
    runname=025GPwc_E3
elif [[ "$exp" == "vajol" ]]; then
    runname=025GPwc_E4
fi
if [[ "$exp" == "vacdi" ]]; then
    runname=025GPsc_E1
elif [[ "$exp" == "vacdj" ]]; then
    runname=025GPsc_E2
elif [[ "$exp" == "vajom" ]]; then
    runname=025GPsc_E3
elif [[ "$exp" == "vajon" ]]; then
    runname=025GPsc_E4
fi

if [[ "$exp" == "vacdo" ]]; then
    runname=081GPwc_E1
elif [[ "$exp" == "vacdp" ]]; then
    runname=081GPwc_E2
elif [[ "$exp" == "vajoo" ]]; then
    runname=081GPwc_E3
elif [[ "$exp" == "vajop" ]]; then
    runname=081GPwc_E4
fi
if [[ "$exp" == "vacdm" ]]; then
    runname=081GPsc_E1
elif [[ "$exp" == "vacdn" ]]; then
    runname=081GPsc_E2
elif [[ "$exp" == "vajoq" ]]; then
    runname=081GPsc_E3
elif [[ "$exp" == "vajor" ]]; then
    runname=081GPsc_E4
fi

if [[ "$exp" == "uaoyy" ]]; then
    runname=121GPwc_E1
elif [[ "$exp" == "uaoyz" ]]; then
    runname=121GPwc_E2
elif [[ "$exp" == "vajos" ]]; then
    runname=121GPwc_E3
elif [[ "$exp" == "vajot" ]]; then
    runname=121GPwc_E4
fi
if [[ "$exp" == "uaoyw" ]]; then
    runname=121GPsc_E1
elif [[ "$exp" == "uaoyx" ]]; then
    runname=121GPsc_E2
elif [[ "$exp" == "vajou" ]]; then
    runname=121GPsc_E3
elif [[ "$exp" == "vajov" ]]; then
    runname=121GPsc_E4
fi

if [[ "$exp" == "vacdq" ]]; then
    runname=242GP_E0
elif [[ "$exp" == "vacdr" ]]; then
    runname=242GP_E1
elif [[ "$exp" == "vacds" ]]; then
    runname=242GP_E2
elif [[ "$exp" == "vacdy" ]]; then
    runname=242GP_E3
elif [[ "$exp" == "vacdz" ]]; then
    runname=242GP_E4
fi
if [[ "$exp" == "vacdt" ]]; then
    runname=allAMZ_E0
elif [[ "$exp" == "vacdu" ]]; then
    runname=allAMZ_E1
elif [[ "$exp" == "vacdv" ]]; then
    runname=allAMZ_E2
elif [[ "$exp" == "vacdw" ]]; then
    runname=allAMZ_E3
elif [[ "$exp" == "vacdx" ]]; then
    runname=allAMZ_E4
fi

mv ${exp}.tas_pr_monthly_TS.${year_start}_${year_end}.nc ${outdir}/${runname}.tas_pr_monthly_TS.${year_start}_${year_end}.nc

done

#clean up
rm ${workdir}/*

#END
