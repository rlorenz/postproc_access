#!/bin/bash
## \file post_proc_ACCESS_timeseries.sh
#  \author Ruth Lorenz (r.lorenz@unsw.edu.au)
#  \brief create timeseries for UM output which is archived on mdss as monthly *.nc.gz files
#  \usage qsub -V -v -lother=mdss post_proc_ACCESS_monthly.sh

#PBS -q copyq
#PBS -N post_mon
#PBS -j oe
#PBS -l other=mdss
#PBS -l ncpus=1
#PBS -l mem=2152mb
#PBS -l walltime=10:00:00
#PBS -l wd

##-----------------------
#function to determine if year is leapyear (1) or not (0)
##-----------------------

function leapyr ()
{ 
  if [[  ${year}%4 -eq "0" && ${year}%100 -ne "0" || ${year}%400 -eq "0" ]]; then
    local __lpyr=1
  else
    local __lpyr=0
  fi
  echo "$__lpyr"
}

##-----------------------##
## load required modules ##
##-----------------------##

module load netcdf
module load nco/4.3.2
module load cdo
 
##---------------------##
## user specifications ##
##-------------------- ##

exp=uamon
year_start=1978
year_end=2006
archive=$USER/$exp/
workdir=/short/dt6/$USER/postproc/$exp/work/monthly/
outdir=/short/dt6/$USER/postproc/$exp/timeseries/

#infos for global attributes in netcdf
TITLE_RUN="ACCESS1.3b GLACE-CMIP5 output GC1B85\n"
SOURCE="ACCESS1.3b (HadGEM3-CABLE2.0), AMIP, N96\n"
INSTITUTION="ARC Centre of Excellence for Climate System Science,\n model run at NCI National Facility\n"
CONTACT="r.lorenz@unsw.edu.au"

##---------------------##

mkdir /short/dt6/$USER/postproc
mkdir /short/dt6/$USER/postproc/$exp
mkdir ${outdir}
mkdir ${outdir}/yearly
mkdir /short/dt6/$USER/postproc/$exp/work/
mkdir ${workdir}

cd ${workdir}

##-------------------------------------------##
## monthly files:                            ##
##-------------------------------------------##

## get data from mdss, loop over year, unzip
## and concatenate yearly archives          

year=$year_start

     while [  $year -le $year_end ]; do
       #loop over years
       echo "Processing year $year"

            #for first year get december of year before (for seasonal means)
	    if [ $year -eq $year_start ]; then
		 let "year_before=year-1"
		 check if file available on mdss
		 file=`mdss dmls -l ${archive}/$exp.monthly.$year_before-12-00T00.nc.gz | awk '{print $2}'`
		 if [[ "$file" != 1 ]]; then
		     echo "ERROR: missing file $exp.monthly.$year_before-12-00T00.nc.gz  in ${archive}" 1>&2
		     exit 1
		 fi		    
		 mdss get ${archive}/$exp.monthly.$year_before-12-00T00.nc.gz
		 gunzip $exp.monthly.$year_before-12-00T00.nc.gz

	     fi

		for month in 01 02 03 04 05 06 07 08 09 10 11 12
		  do
		  #monthly files, check availability first
                  file_mon=`mdss dmls -l ${archive}/$exp.monthly.$year-$month-00T00.nc.gz | awk '{print $2}'`
                  if [[ "$file_mon" != 1 ]]; then
        	   echo "ERROR: missing file $exp.monthly.$year-$month-00T00.nc.gz in ${archive}" 1>&2
         	  exit 1
                  fi
		  mdss get ${archive}/$exp.monthly.$year-$month-00T00.nc.gz
		  gunzip $exp.monthly.$year-$month-00T00.nc.gz

		  done
		
		  if [ $year -eq $year_start ]; then
		      let "year_before=year-1"
		      ncrcat -O $exp.monthly.$year_before-12-00T00.nc $exp.monthly.$year-??-00T00.nc \
			  ${workdir}/$exp.monthly.dec$year_before-$year.nc
		      ncrcat -O $exp.monthly.$year-??-00T00.nc ${workdir}/$exp.monthly.$year.nc
		  elif [ $year -eq $year_end ]; then
		      ncrcat -O $exp.monthly.$year-??-00T00.nc ${workdir}/$exp.monthly.$year.nc
		      rm $exp.monthly.$year-12-00T00.nc
		      ncrcat -O $exp.monthly.$year-??-00T00.nc ${workdir}/$exp.monthly.$year-dec.nc
		  else 
		      ncrcat -O $exp.monthly.$year-??-00T00.nc ${workdir}/$exp.monthly.$year.nc
		  fi

                  # clean up
                  rm $exp.monthly.????-??-00T00.nc

		let year=year+1
	done #year

#Create timeseries over whole time frame
# monthly mean climatology and seasonal means for monthly files

echo "Create seasonal averages and monthly timeseries..."

#create timeseries and monthly means
ncrcat -O $exp.monthly.????.nc $exp.monthly_TS.${year_start}_${year_end}.nc
#add global attributes
ncatted -h -O -a title,global,a,c,$TITLE_RUN $exp.monthly_TS.${year_start}_${year_end}.nc
ncatted -h -O -a institution,global,a,c,$INSTITUTION $exp.monthly_TS.${year_start}_${year_end}.nc
ncatted -h -O -a source,global,a,c,$SOURCE $exp.monthly_TS.${year_start}_${year_end}.nc
ncatted -h -O -a  contact,global,a,c,$CONTACT  $exp.monthly_TS.${year_start}_${year_end}.nc
ncatted -h -O -a  comment,global,a,c,"Monthly timeseries" $exp.monthly_TS.${year_start}_${year_end}.nc

ncea -O $exp.monthly.????.nc $exp.monthly_MSC.${year_start}_${year_end}.nc
#add global attributes
ncatted -h -O -a title,global,a,c,$TITLE_RUN"\n" $exp.monthly_MSC.${year_start}_${year_end}.nc
ncatted -h -O -a institution,global,a,c,$INSTITUTION  $exp.monthly_MSC.${year_start}_${year_end}.nc
ncatted -h -O -a source,global,a,c,$SOURCE"\n" $exp.monthly_MSC.${year_start}_${year_end}.nc
ncatted -h -O -a  contact,global,a,c,$CONTACT $exp.monthly_MSC.${year_start}_${year_end}.nc
ncatted -h -O -a  comment,global,a,c,"Monthly climatology" $exp.monthly_MSC.${year_start}_${year_end}.nc

#copy yearly files to outdir/yearly for future calculation of other timeperiods
cp $exp.monthly.????.nc ${outdir}/yearly/

#create seasonal averages
rm $exp.monthly.$year_end.nc
rm $exp.monthly.$year_start.nc
ncrcat -O $exp.monthly.dec$year_before-$year_start.nc $exp.monthly.????.nc $exp.monthly.$year_end-dec.nc \
    $exp.monthly_TSseas.${year_start}_${year_end}.nc

cdo yseasmean $exp.monthly_TSseas.${year_start}_${year_end}.nc $exp.seasonal_means.${year_start}_${year_end}.nc
#add global attributes
ncatted -h -O -a title,global,a,c,$TITLE_RUN $exp.seasonal_means.${year_start}_${year_end}.nc
ncatted -h -O -a institution,global,a,c,$INSTITUTION $exp.seasonal_means.${year_start}_${year_end}.nc
ncatted -h -O -a source,global,a,c,$SOURCE $exp.seasonal_means.${year_start}_${year_end}.nc
ncatted -h -O -a  contact,global,a,c,$CONTACT $exp.seasonal_means.${year_start}_${year_end}.nc
ncatted -h -O -a  comment,global,a,c,"Seasonal means over all years" $exp.seasonal_means.${year_start}_${year_end}.nc

# clean up
rm $exp.monthly.????.nc
rm $exp.monthly.dec$year_before-$year_start.nc
rm $exp.monthly.$year_end-dec.nc
rm $exp.monthly_TSseas.${year_start}_${year_end}.nc

#create yearly means
cdo yearavg $exp.monthly_TS.${year_start}_${year_end}.nc $exp.yearly_means.${year_start}_${year_end}.nc
#add global attributes
ncatted -h -O -a title,global,a,c,$TITLE_RUN $exp.yearly_means.${year_start}_${year_end}.nc
ncatted -h -O -a institution,global,a,c,$INSTITUTION  $exp.yearly_means.${year_start}_${year_end}.nc
ncatted -h -O -a source,global,a,c,$SOURCE $exp.yearly_means.${year_start}_${year_end}.nc
ncatted -h -O -a  contact,global,a,c,$CONTACT $exp.yearly_means.${year_start}_${year_end}.nc
ncatted -h -O -a  comment,global,a,c,"Yearly means" $exp.yearly_means.${year_start}_${year_end}.nc

## zip and move monthly timeseries to output
gzip $exp.monthly_TS.${year_start}_${year_end}.nc
gzip $exp.monthly_MSC.${year_start}_${year_end}.nc
gzip $exp.seasonal_means.${year_start}_${year_end}.nc
gzip $exp.yearly_means.${year_start}_${year_end}.nc

# Move output data to OUTDIR space.
echo "The monthly output files are now moved to ${outdir}."

mv $exp.monthly_TS.${year_start}_${year_end}.nc.gz ${outdir}
mv $exp.monthly_MSC.${year_start}_${year_end}.nc.gz ${outdir}
mv $exp.seasonal_means.${year_start}_${year_end}.nc.gz ${outdir}
mv $exp.yearly_means.${year_start}_${year_end}.nc.gz ${outdir}

#clean up
rm ${workdir}/*

#END
