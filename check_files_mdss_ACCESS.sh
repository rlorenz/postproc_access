#!/bin/bash
## \file check_files_mdss_ACCESS.sh
##  \author Ruth Lorenz (r.lorenz@unsw.edu.au)
##  \brief check availabiliy of UM output which is archived on mdss as monthly *.nc.gz files
##  \usage qsub -V -v -lother=mdss check_files_mdss_ACCESS.sh

#PBS -q copyq
#PBS -N check_files
#PBS -j oe
#PBS -l other=mdss
#PBS -l ncpus=1
###PBS -l mem=1536mb
#PBS -l walltime=02:00:00
#PBS -l wd

##---------------------##
## user specifications ##
##-------------------- ##

exp=vacdl
year_start=1975
year_end=2012
archive=$USER/$exp/

cd /home/561/rzl561/scripts/post_proc/ACCESS/

##-----------------------------------------------##
## monthly, daily, 3 hourly, and/or hourly files ##
##-----------------------------------------------##

#for timeperiod in monthly daily 3hourly hourly
for timeperiod in monthly daily
  do

  year=$year_start

     while [  $year -le $year_end ]; do
       #loop over years
       echo "Processing year $year for timeperiod $timeperiod"

		for month in 01 02 03 04 05 06 07 08 09 10 11 12
		  do
		  file=`mdss dmls -l ${archive}/$exp.$timeperiod.$year-$month-00T00.nc.gz | awk '{print $2}'`
		  if [[ "$file" != 1 ]]; then
		      echo "ERROR: missing file $exp.$timeperiod.$year-$month-00T00.nc.gz in ${archive}" 1>&2
		  fi
		  
		  done #month

		  let year=year+1
     done #year

done #timeperiod

#END
