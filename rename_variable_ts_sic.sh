#!/bin/bash
## \file rename_variable_ts_sic.sh
#  \author Ruth Lorenz (r.lorenz@unsw.edu.au)
#  \brief rename field508 to ts_sic in files where wrong
#  \usage qsub -V -v -lother=mdss rename_variable_ts_sic.sh

#PBS -q copyq
#PBS -N rename
#PBS -j oe
#PBS -l other=mdss
#PBS -l ncpus=1
#PBS -l mem=1536mb
#PBS -l walltime=00:15:00
#PBS -l wd

##-----------------------##
## load required modules ##
##-----------------------##

module load netcdf
module load nco
module load cdo
 
##---------------------##
## user specifications ##
##-------------------- ##
exp=uamoa
year_start=1953
year_end=1953
archive=$USER/$exp/
workdir=/short/dt6/$USER/postproc/work/

cd ${workdir}

##-------------------------------------------##
## monthly files:                            ##
##-------------------------------------------##

year=$year_start

     while [  $year -le $year_end ]; do
       #loop over years
       echo "Processing year $year"

		for month in 01 02 03 04 05 06 07 08 09 10 11 12
		  do
		  #monthly files, check availability first
                  #file_mon=`mdss dmls -l ${archive}/$exp.monthly.$year-$month-00T00.nc.gz | awk '{print $2}'`
                  file_day=`mdss dmls -l ${archive}/$exp.daily.$year-$month-00T00.nc.gz | awk '{print $2}'`
                  #if [[ "$file_mon" != 1 ]]; then
        	  # echo "ERROR: missing file $exp.monthly.$year-$month-00T00.nc.gz in ${archive}" 1>&2
         	  # exit 1
                  #fi
                  if [[ "$file_day" != 1 ]]; then
        	   echo "ERROR: missing file $exp.daily.$year-$month-00T00.nc.gz in ${archive}" 1>&2
         	   exit 1
                  fi
		  
		  #echo "getting monthly file"
		  #mdss get ${archive}/$exp.monthly.$year-$month-00T00.nc.gz
		  #gunzip $exp.monthly.$year-$month-00T00.nc.gz
		  #rm $exp.monthly.$year-$month-00T00.nc.gz
		  #ncrename -h -O -v field508,ts_sic $exp.monthly.$year-$month-00T00.nc $exp.monthly.$year-$month-00T00.nc
		  #ncatted -h -a standard_name,ts_sic,o,c,"surface_temperature_where_sea-ice" $exp.monthly.$year-$month-00T00.nc
		  #ncatted -h -a name,ts_sic,o,c,"ts_sic" $exp.monthly.$year-$month-00T00.nc
		  #echo "putting altered monthly file to mdss"
		  #gzip $exp.monthly.$year-$month-00T00.nc
		  #mdss put $exp.monthly.$year-$month-00T00.nc.gz $archive

		  echo "getting daily file"
		  mdss get ${archive}/$exp.daily.$year-$month-00T00.nc.gz
		  gunzip $exp.daily.$year-$month-00T00.nc.gz
		  rm $exp.daily.$year-$month-00T00.nc.gz
		  ncrename -h -O -v field508,ts_sic $exp.daily.$year-$month-00T00.nc $exp.daily.$year-$month-00T00.nc
		  ncatted -h -a standard_name,ts_sic,o,c,"surface_temperature_where_sea-ice" $exp.daily.$year-$month-00T00.nc
		  ncatted -h -a name,ts_sic,o,c,"ts_sic" $exp.daily.$year-$month-00T00.nc
		  echo "putting altered daily file to mdss"		  
		  gzip $exp.daily.$year-$month-00T00.nc
		  mdss put $exp.daily.$year-$month-00T00.nc.gz $archive

		  done
		let year=year+1
	done #while

#END
