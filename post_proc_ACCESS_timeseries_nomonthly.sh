#!/bin/bash
## \file post_proc_ACCESS_timeseries_nomonthly.sh
#  \author Ruth Lorenz (r.lorenz@unsw.edu.au)
#  \brief create timeseries for UM output which is archived on mdss as monthly *.nc.gz files
#  \usage qsub -V -v -lother=mdss post_proc_ACCESS_timeseries_nomonthly.sh

#PBS -q copyq
#PBS -N post_day
#PBS -P dt6
#PBS -j oe
#PBS -l other=mdss
#PBS -l ncpus=1
#PBS -l mem=1536mb
#PBS -l walltime=10:00:00
#PBS -l wd

##-----------------------
#function to determine if year is leapyear (1) or not (0)
##-----------------------

function leapyr ()
{ 
  if [[  ${year}%4 -eq "0" && ${year}%100 -ne "0" || ${year}%400 -eq "0" ]]; then
    local __lpyr=1
  else
    local __lpyr=0
  fi
  echo "$__lpyr"
}

##-----------------------##
## load required modules ##
##-----------------------##

module load netcdf
module load nco
module load cdo
 
##---------------------##
## user specifications ##
##-------------------- ##

exp=uaoym
year_start=1978
year_end=2011
archive=$USER/$exp/
workdir=/short/dt6/$USER/postproc/$exp/work/daily/
outdir=/short/dt6/$USER/postproc/$exp/timeseries/

#infos for global attributes in netcdf
TITLE_RUN="ACCESS1.3b output from Land use change experiments,\n 049GPsc E1"
SOURCE="ACCESS1.3b (HadGEM3-CABLE2.0), AMIP, N96\n"
INSTITUTION="ARC Centre of Excellence for Climate System Science,\n model run at NCI National Facility\n"
CONTACT="r.lorenz@unsw.edu.au"

##---------------------##

mkdir /short/dt6/$USER/postproc
mkdir /short/dt6/$USER/postproc/$exp
mkdir ${outdir}
mkdir ${outdir}/yearly
mkdir ${workdir}

cd ${workdir}

##-------------------------------------------##
## daily, 3 hourly, and hourly files:        ##
##-------------------------------------------##

#for timeperiod in daily 3hourly hourly
for timeperiod in daily
  do

  year=$year_start

     while [  $year -le $year_end ]; do
       #loop over years
       echo "Processing year $year for timeperiod $timeperiod"

		for month in 01 02 03 04 05 06 07 08 09 10 11 12
		  do
		  file=`mdss dmls -l ${archive}/$exp.$timeperiod.$year-$month-00T00.nc.gz | awk '{print $2}'`
		  if [[ "$file" != 1 ]]; then
		      echo "ERROR: missing file $exp.$timeperiod.$year-$month-00T00.nc.gz in ${archive}" 1>&2
		      exit 2
		  fi
		  mdss get ${archive}/$exp.$timeperiod.$year-$month-00T00.nc.gz
		  gunzip $exp.$timeperiod.$year-$month-00T00.nc.gz
		  
		  done #month

		  ncrcat -O $exp.$timeperiod.$year-??-00T00.nc $exp.$timeperiod.$year.nc
		  rm $exp.$timeperiod.$year-??-00T00.nc

		  #if daily, extract tmax and tmin for monthly means
		  if [[ "$timeperiod" == "daily" ]]; then
		      ncks -O -v tasmax,tasmin $exp.$timeperiod.$year.nc $exp.${timeperiod}_tmax_tmin.$year.nc
		      
		      #take december from year before but not last december for seasonal means
		      if [ $year -eq $year_start ]; then
		      let "year_before=year-1"
		      mdss get ${archive}/$exp.$timeperiod.${year_before}-12-00T00.nc.gz
		      gunzip $exp.$timeperiod.${year_before}-12-00T00.nc.gz
		      ncks -O -v tasmax,tasmin $exp.$timeperiod.${year_before}-12-00T00.nc $exp.${timeperiod}_tmax_tmin.dec$year_before.nc
		      ncrcat -O $exp.${timeperiod}_tmax_tmin.dec$year_before.nc $exp.${timeperiod}_tmax_tmin.$year.nc $exp.${timeperiod}_tmax_tmin.dec${year_before}_$year.nc
		      cdo monmean $exp.${timeperiod}_tmax_tmin.dec${year_before}_$year.nc $exp.monmean_tmax_tmin.dec${year_before}_$year.nc
		      elif [ $year -eq $year_end ]; then
			  let endT=333+$(leapyr)
			  echo "$year is leapyear?: $(leapyr)"
		      ncks -O -d time,0,$endT $exp.${timeperiod}_tmax_tmin.$year_end.nc $exp.${timeperiod}_tmax_tmin.$year_end-dec.nc
		      cdo monmean $exp.${timeperiod}_tmax_tmin.$year_end.nc $exp.monmean_tmax_tmin.$year_end-dec.nc
		      fi
		      cdo monmean $exp.${timeperiod}_tmax_tmin.$year.nc $exp.monmean_tmax_tmin.$year.nc

		  fi

		  #if hourly -> separate variables
		  if [[ "$timeperiod" == "hourly" ]]; then
		      ncks -O -x -v hfls,tas,hfss $exp.$timeperiod.$year.nc $exp.${timeperiod}_pr.$year.nc
		      ncks -O -x -v pr,tas,hfss $exp.$timeperiod.$year.nc $exp.${timeperiod}_hfls.$year.nc
		      ncks -O -x -v pr,tas,hfls $exp.$timeperiod.$year.nc $exp.${timeperiod}_hfss.$year.nc
		      ncks -O -x -v pr,hfls,hfss $exp.$timeperiod.$year.nc $exp.${timeperiod}_tas.$year.nc
		  fi

		  let year=year+1
     done #year

  # Create timeseries
  echo "Create $timeperiod timeseries..."
  if [[ "$timeperiod" == "hourly" ]]; then
  ncrcat -O $exp.${timeperiod}_pr.????.nc $exp.${timeperiod}_pr.${year_start}_${year_end}.nc
  #add global attributes
  ncatted -h -O -a title,global,a,c,$TITLE_RUN $exp.${timeperiod}_pr.${year_start}_${year_end}.nc
  ncatted -h -O -a institution,global,a,c,$INSTITUTION $exp.${timeperiod}_pr.${year_start}_${year_end}.nc 
  ncatted -h -O -a source,global,a,c,$SOURCE $exp.${timeperiod}_pr.${year_start}_${year_end}.nc
  ncatted -h -O -a  contact,global,a,c,$CONTACT $exp.${timeperiod}_pr.${year_start}_${year_end}.nc

  ncrcat -O $exp.${timeperiod}_hfls.????.nc $exp.${timeperiod}_hfls.${year_start}_${year_end}.nc
  #add global attributes
  ncatted -h -O -a title,global,a,c,$TITLE_RUN $exp.${timeperiod}_hfls.${year_start}_${year_end}.nc
  ncatted -h -O -a institution,global,a,c,$INSTITUTION $exp.${timeperiod}_hfls.${year_start}_${year_end}.nc  
  ncatted -h -O -a source,global,a,c,$SOURCE $exp.${timeperiod}_hfls.${year_start}_${year_end}.nc
  ncatted -h -O -a  contact,global,a,c,$CONTACT $exp.${timeperiod}_hfls.${year_start}_${year_end}.nc

  ncrcat -O $exp.${timeperiod}_hfss.????.nc $exp.${timeperiod}_hfss.${year_start}_${year_end}.nc
  #add global attributes
  ncatted -h -O -a title,global,a,c,$TITLE_RUN $exp.${timeperiod}_hfss.${year_start}_${year_end}.nc
  ncatted -h -O -a institution,global,a,c,$INSTITUTION $exp.${timeperiod}_hfss.${year_start}_${year_end}.nc
  ncatted -h -O -a source,global,a,c,$SOURCE $exp.${timeperiod}_hfss.${year_start}_${year_end}.nc
  ncatted -h -O -a  contact,global,a,c,$CONTACT $exp.${timeperiod}_hfss.${year_start}_${year_end}.nc

  ncrcat -O $exp.${timeperiod}_tas.????.nc $exp.${timeperiod}_tas.${year_start}_${year_end}.nc
  #add global attributes
  ncatted -h -O -a title,global,a,c,$TITLE_RUN $exp.${timeperiod}_tas.${year_start}_${year_end}.nc
  ncatted -h -O -a institution,global,a,c,$INSTITUTION  $exp.${timeperiod}_tas.${year_start}_${year_end}.nc
  ncatted -h -O -a source,global,a,c,$SOURCE $exp.${timeperiod}_tas.${year_start}_${year_end}.nc
  ncatted -h -O -a  contact,global,a,c,$CONTACT $exp.${timeperiod}_tas.${year_start}_${year_end}.nc

  elif [[ "$timeperiod" == "daily" ]]; then
  ncrcat -O $exp.monmean_tmax_tmin.????.nc $exp.monmean_tmax_tmin.${year_start}_${year_end}.nc
  #add global attributes
  ncatted -h -O -a title,global,a,c,$TITLE_RUN $exp.monmean_tmax_tmin.${year_start}_${year_end}.nc
  ncatted -h -O -a institution,global,a,c,$INSTITUTION  $exp.monmean_tmax_tmin.${year_start}_${year_end}.nc
  ncatted -h -O -a source,global,a,c,$SOURCE $exp.monmean_tmax_tmin.${year_start}_${year_end}.nc
  ncatted -h -O -a  contact,global,a,c,$CONTACT $exp.monmean_tmax_tmin.${year_start}_${year_end}.nc

  rm $exp.monmean_tmax_tmin.$year_start.nc
  rm $exp.monmean_tmax_tmin.$year_end.nc
  ncrcat -O $exp.monmean_tmax_tmin.dec${year_before}_????.nc $exp.monmean_tmax_tmin.????.nc $exp.monmean_tmax_tmin.$year_end-dec.nc $exp.monmean_tmax_tmin_forseas.${year_start}_${year_end}.nc
  cdo yseasmean $exp.monmean_tmax_tmin_forseas.${year_start}_${year_end}.nc $exp.seasmean_tmax_tmin.${year_start}_${year_end}.nc
  #add global attributes
  ncatted -h -O -a title,global,a,c,$TITLE_RUN $exp.seasmean_tmax_tmin.${year_start}_${year_end}.nc
  ncatted -h -O -a institution,global,a,c,$INSTITUTION  $exp.seasmean_tmax_tmin.${year_start}_${year_end}.nc
  ncatted -h -O -a source,global,a,c,$SOURCE $exp.seasmean_tmax_tmin.${year_start}_${year_end}.nc
  ncatted -h -O -a  contact,global,a,c,$CONTACT $exp.seasmean_tmax_tmin.${year_start}_${year_end}.nc

  ncrcat -O $exp.${timeperiod}.????.nc $exp.${timeperiod}_TS.${year_start}_${year_end}.nc
  #add global attributes
  ncatted -h -O -a title,global,a,c,$TITLE_RUN $exp.${timeperiod}_TS.${year_start}_${year_end}.nc
  ncatted -h -O -a institution,global,a,c,$INSTITUTION  $exp.${timeperiod}_TS.${year_start}_${year_end}.nc
  ncatted -h -O -a source,global,a,c,$SOURCE $exp.${timeperiod}_TS.${year_start}_${year_end}.nc
  ncatted -h -O -a  contact,global,a,c,$CONTACT $exp.${timeperiod}_TS.${year_start}_${year_end}.nc

  else
  ncrcat -O $exp.${timeperiod}.????.nc $exp.${timeperiod}_TS.${year_start}_${year_end}.nc
  #add global attributes
  ncatted -h -O -a title,global,a,c,$TITLE_RUN $exp.${timeperiod}_TS.${year_start}_${year_end}.nc
  ncatted -h -O -a institution,global,a,c,$INSTITUTION  $exp.${timeperiod}_TS.${year_start}_${year_end}.nc
  ncatted -h -O -a source,global,a,c,$SOURCE $exp.${timeperiod}_TS.${year_start}_${year_end}.nc
  ncatted -h -O -a  contact,global,a,c,$CONTACT $exp.${timeperiod}_TS.${year_start}_${year_end}.nc

  fi

  #clean up 
  #rm $exp.$timeperiod.????.nc
  mv $exp.$timeperiod.????.nc ${outdir}/yearly/

  #prepare daily Tmax,Tmin and PR for climdex
  if  [[ "$timeperiod" == "daily" ]]; then
      ncks -h -O -v tasmin ${exp}.daily_TS.${year_start}_${year_end}.nc ${exp}_TN_${year_start}-${year_end}.nc
      ncrename -h -v tasmin,Tmin ${exp}_TN_${year_start}-${year_end}.nc
      cdo setreftime,1950-01-01,00:00:00,days ${exp}_TN_${year_start}-${year_end}.nc ${exp}_TN_${year_start}-${year_end}_ref1950.nc
      ncatted -h -a missing_value,Tmin,d,, ${exp}_TN_${year_start}-${year_end}_ref1950.nc
      ncatted -h -a _FillValue,Tmin,d,, ${exp}_TN_${year_start}-${year_end}_ref1950.nc
      cdo subc,273.15 ${exp}_TN_${year_start}-${year_end}_ref1950.nc ${exp}_TN_${year_start}-${year_end}_degC.nc
      rm ${exp}_TN_${year_start}-${year_end}_ref1950.nc
      #add global attributes
      ncatted -h -O -a title,global,a,c,$TITLE_RUN ${exp}_TN_${year_start}-${year_end}_degC.nc
      ncatted -h -O -a institution,global,a,c,$INSTITUTION  ${exp}_TN_${year_start}-${year_end}_degC.nc
      ncatted -h -O -a source,global,a,c,$SOURCE ${exp}_TN_${year_start}-${year_end}_degC.nc
      ncatted -h -O -a  contact,global,a,c,$CONTACT ${exp}_TN_${year_start}-${year_end}_degC.nc

      ncks -h -O -v tasmax ${exp}.daily_TS.${year_start}_${year_end}.nc ${exp}_TX_${year_start}-${year_end}.nc
      ncrename -h -v tasmax,Tmax ${exp}_TX_${year_start}-${year_end}.nc
      cdo setreftime,1950-01-01,00:00:00,days ${exp}_TX_${year_start}-${year_end}.nc ${exp}_TX_${year_start}-${year_end}_ref1950.nc
      ncatted -h -a missing_value,Tmax,d,, ${exp}_TX_${year_start}-${year_end}_ref1950.nc
      ncatted -h -a _FillValue,Tmax,d,, ${exp}_TX_${year_start}-${year_end}_ref1950.nc
      cdo subc,273.15 ${exp}_TX_${year_start}-${year_end}_ref1950.nc ${exp}_TX_${year_start}-${year_end}_degC.nc
      rm ${exp}_TX_${year_start}-${year_end}_ref1950.nc
      #add global attributes
      ncatted -h -O -a title,global,a,c,$TITLE_RUN ${exp}_TX_${year_start}-${year_end}_degC.nc
      ncatted -h -O -a institution,global,a,c,$INSTITUTION  ${exp}_TX_${year_start}-${year_end}_degC.nc
      ncatted -h -O -a source,global,a,c,$SOURCE ${exp}_TX_${year_start}-${year_end}_degC.nc
      ncatted -h -O -a  contact,global,a,c,$CONTACT ${exp}_TX_${year_start}-${year_end}_degC.nc

      ncks -h -O -v pr ${exp}.daily_TS.${year_start}_${year_end}.nc ${exp}_PR_${year_start}-${year_end}.nc
      ncrename -h -v pr,prcp ${exp}_PR_${year_start}-${year_end}.nc
      cdo setreftime,1950-01-01,00:00:00,days ${exp}_PR_${year_start}-${year_end}.nc ${exp}_PR_${year_start}-${year_end}_ref1950.nc
      ncatted -h -a missing_value,prcp,d,, ${exp}_PR_${year_start}-${year_end}_ref1950.nc
      ncatted -h -a _FillValue,prcp,d,, ${exp}_PR_${year_start}-${year_end}_ref1950.nc
      cdo mulc,86400 ${exp}_PR_${year_start}-${year_end}_ref1950.nc  ${exp}_PR_${year_start}-${year_end}_mm.nc
      rm ${exp}_PR_${year_start}-${year_end}_ref1950.nc
      #add global attributes
      ncatted -h -O -a title,global,a,c,$TITLE_RUN ${exp}_PR_${year_start}-${year_end}_mm.nc
      ncatted -h -O -a institution,global,a,c,$INSTITUTION  ${exp}_PR_${year_start}-${year_end}_mm.nc
      ncatted -h -O -a source,global,a,c,$SOURCE ${exp}_PR_${year_start}-${year_end}_mm.nc
      ncatted -h -O -a  contact,global,a,c,$CONTACT ${exp}_PR_${year_start}-${year_end}_mm.nc

  fi

  #compress file
  if [[ "$timeperiod" == "hourly" ]]; then
      gzip $exp.${timeperiod}_pr.${year_start}_${year_end}.nc  
      gzip $exp.${timeperiod}_hfls.${year_start}_${year_end}.nc
      gzip $exp.${timeperiod}_tas.${year_start}_${year_end}.nc
      gzip $exp.${timeperiod}_hfss.${year_start}_${year_end}.nc
  elif [[ "$timeperiod" == "daily" ]]; then
      gzip $exp.seasmean_tmax_tmin.${year_start}_${year_end}.nc
      gzip $exp.monmean_tmax_tmin.${year_start}_${year_end}.nc
      gzip $exp.${timeperiod}_TS.${year_start}_${year_end}.nc
  else
      gzip $exp.${timeperiod}_TS.${year_start}_${year_end}.nc
  fi

  # Move output data to output DIR.
  echo "The $timeperiod output files are now moved to ${outdir}."

  if [[ "$timeperiod" == "hourly" ]]; then
  mv $exp.${timeperiod}_pr.${year_start}_${year_end}.nc.gz ${outdir}
  mv $exp.${timeperiod}_hfls.${year_start}_${year_end}.nc.gz ${outdir}
  mv $exp.${timeperiod}_hfss.${year_start}_${year_end}.nc.gz ${outdir}
  mv $exp.${timeperiod}_tas.${year_start}_${year_end}.nc.gz ${outdir}
  elif [[ "$timeperiod" == "daily" ]]; then
  mv ${exp}_TN_${year_start}-${year_end}_degC.nc ${outdir}
  mv ${exp}_TX_${year_start}-${year_end}_degC.nc ${outdir}
  mv ${exp}_PR_${year_start}-${year_end}_mm.nc ${outdir}
  mv $exp.monmean_tmax_tmin.${year_start}_${year_end}.nc.gz ${outdir}
  mv $exp.seasmean_tmax_tmin.${year_start}_${year_end}.nc.gz ${outdir}
  mv $exp.${timeperiod}_TS.${year_start}_${year_end}.nc.gz ${outdir}
  else
  mv $exp.${timeperiod}_TS.${year_start}_${year_end}.nc.gz ${outdir}
  fi

done #timeperiod

#clean up
rm ${workdir}/*

#END
