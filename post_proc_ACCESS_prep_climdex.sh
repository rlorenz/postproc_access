#!/bin/bash
## \file post_proc_ACCESS_timeseries.sh
#  \author Ruth Lorenz (r.lorenz@unsw.edu.au)
#  \brief create timeseries for UM output which is archived on mdss as monthly *.nc.gz files
#  \usage qsub post_proc_ACCESS_prep_climdex.sh

#PBS -q copyq
#PBS -N postproc
#PBS -j oe
#PBS -l other=mdss
#PBS -l ncpus=1
#PBS -l mem=1536mb
#PBS -l walltime=2:00:00
#PBS -l wd

##-----------------------
#function to determine if year is leapyear (1) or not (0)
##-----------------------

function leapyr ()
{ 
  if [[  ${year}%4 -eq "0" && ${year}%100 -ne "0" || ${year}%400 -eq "0" ]]; then
    local __lpyr=1
  else
    local __lpyr=0
  fi
  echo "$__lpyr"
}

##-----------------------##
## load required modules ##
##-----------------------##

module load netcdf
module load nco
module load cdo
 
##---------------------##
## user specifications ##
##-------------------- ##

exp=uamoh
year_start=1951
year_end=2100
archive=$USER/$exp/
workdir=/short/dt6/$USER/postproc/$exp/work/daily/
outdir=/short/dt6/$USER/postproc/$exp/timeseries/

##---------------------##

mkdir /short/dt6/$USER/postproc
mkdir /short/dt6/$USER/postproc/$exp
mkdir ${outdir}
mkdir ${outdir}/yearly
mkdir ${workdir}

cd ${workdir}

##-------------------------------------------##
## daily, 3 hourly, and hourly files:        ##
##-------------------------------------------##

for timeperiod in daily
  do


  #prepare daily Tmax,Tmin and PR for climdex
      ncks -O -v tasmin ${exp}.daily_TS.${year_start}_${year_end}.nc ${exp}_TN_${year_start}-${year_end}.nc
      ncrename -v tasmin,Tmin ${exp}_TN_${year_start}-${year_end}.nc
      cdo setreftime,1950-01-01,00:00:00,days ${exp}_TN_${year_start}-${year_end}.nc ${exp}_TN_${year_start}-${year_end}_ref1950.nc
      ncatted -a missing_value,Tmin,d,, ${exp}_TN_${year_start}-${year_end}_ref1950.nc
      ncatted -a _FillValue,Tmin,d,, ${exp}_TN_${year_start}-${year_end}_ref1950.nc
      cdo subc,273.15 ${exp}_TN_${year_start}-${year_end}_ref1950.nc ${exp}_TN_${year_start}-${year_end}_degC.nc
      rm ${exp}_TN_${year_start}-${year_end}_ref1950.nc

      ncks -O -v tasmax ${exp}.daily_TS.${year_start}_${year_end}.nc ${exp}_TX_${year_start}-${year_end}.nc
      ncrename -v tasmax,Tmax ${exp}_TX_${year_start}-${year_end}.nc
      cdo setreftime,1950-01-01,00:00:00,days ${exp}_TX_${year_start}-${year_end}.nc ${exp}_TX_${year_start}-${year_end}_ref1950.nc
      ncatted -a missing_value,Tmax,d,, ${exp}_TX_${year_start}-${year_end}_ref1950.nc
      ncatted -a _FillValue,Tmax,d,, ${exp}_TX_${year_start}-${year_end}_ref1950.nc
      cdo subc,273.15 ${exp}_TX_${year_start}-${year_end}_ref1950.nc ${exp}_TX_${year_start}-${year_end}_degC.nc
      rm ${exp}_TX_${year_start}-${year_end}_ref1950.nc

      ncks -O -v pr ${exp}.daily_TS.${year_start}_${year_end}.nc ${exp}_PR_${year_start}-${year_end}.nc
      ncrename -v pr,prcp ${exp}_PR_${year_start}-${year_end}.nc
      cdo setreftime,1950-01-01,00:00:00,days ${exp}_PR_${year_start}-${year_end}.nc ${exp}_PR_${year_start}-${year_end}_ref1950.nc
      ncatted -a missing_value,prcp,d,, ${exp}_PR_${year_start}-${year_end}_ref1950.nc
      ncatted -a _FillValue,prcp,d,, ${exp}_PR_${year_start}-${year_end}_ref1950.nc
      cdo mulc,86400 ${exp}_PR_${year_start}-${year_end}_ref1950.nc  ${exp}_PR_${year_start}-${year_end}_mm.nc
      rm ${exp}_PR_${year_start}-${year_end}_ref1950.nc

  # Move output data to output DIR.
  echo "The $timeperiod output files are now moved to ${outdir}."

  mv ${exp}_TN_${year_start}-${year_end}_degC.nc ${outdir}
  mv ${exp}_TX_${year_start}-${year_end}_degC.nc ${outdir}
  mv ${exp}_PR_${year_start}-${year_end}_mm.nc ${outdir}

done #timeperiod

#clean up
rm ${workdir}/*

#END
