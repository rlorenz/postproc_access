#!/bin/bash
## \file GCmon_formatting.sh
#  \author Ruth Lorenz (r.lorenz@unsw.edu.au)
#  \brief put daily GLACE output in correct format, output is archived on mdss
#  \usage qsub -V -v -lother=mdss GCday_formatting.sh

#PBS -q copyq
#PBS -N GCday_format
#PBS -j oe
#PBS -l other=mdss
#PBS -l ncpus=1
#PBS -l mem=1536mb
#PBS -l walltime=10:00:00
#PBS -l wd

##-----------------------##
## load required modules ##
##-----------------------##

module load netcdf
module load nco
module load cdo
 
##---------------------##
## user specifications ##
##-------------------- ##

exp=uamoa
year_start=1950
year_end=2100
archive=$USER/$exp/
workdir=/short/dt6/$USER/postproc/$exp/work/daily/
outdir=/short/dt6/$USER/postproc/$exp/timeseries/GCday/

#determine final experiment name
if [ $exp == "uamoa" ]; then
    expname=CTL
elif [ $exp == "uamom" ]; then
    expname=GC1A85
elif [ $exp == "uamon" ]; then
    expname=GC1B85
elif [ $exp == "uamol" ]; then
    expname=GC1C85
fi

##---------------------##

mkdir /short/dt6/$USER/postproc
mkdir /short/dt6/$USER/postproc/$exp
mkdir /short/dt6/$USER/postproc/$exp/timeseries/
mkdir ${outdir}
mkdir ${workdir}

cd ${workdir}

##-------------------------------------------##
## daily files:                            ##
##-------------------------------------------##

## get data from mdss, loop over year, unzip
## and concatenate yearly archives 

year=${year_start}

while [  $year -le $year_end ]; do
#loop over years
    echo "Processing year $year"
       
    for month in 01 02 03 04 05 06 07 08 09 10 11 12
    do
	#daily files, check availability first
	file_day=`mdss dmls -l ${archive}/$exp.daily.$year-$month-00T00.nc.gz | awk '{print $2}'`
	if [[ "$file_day" != 1 ]]; then
	    echo "ERROR: missing file $exp.daily.$year-$month-00T00.nc.gz in ${archive}" 1>&2
	    exit 1
	    fi

	mdss get ${archive}/$exp.daily.$year-$month-00T00.nc.gz
	gunzip $exp.daily.$year-$month-00T00.nc.gz
	done #month

# concatenate yearly archives
ncrcat -h -O $exp.daily.$year-??-00T00.nc ${workdir}/$exp.daily.$year.nc

# clean up
rm $exp.daily.????-??-00T00.nc

#bring variables in correct format and store one file per variable
ncatted -h -a long_name,lon,o,c,"longitude" $exp.daily.$year.nc
ncatted -h -a long_name,lat,o,c,"latitude" $exp.daily.$year.nc

ncatted -h -O -a _FillValue,,o,f,1.0e20 $exp.daily.$year.nc
ncatted -h -O -a _FillValue,time,d,, $exp.daily.$year.nc

#Soil moisture: calculate mrsos, mrso, mrfso, mrlsl from original output (mrsos,soiice)
ncks -h -O -v mrsos $exp.daily.$year.nc tmp_GCday_${year}_mrsos.nc
ncrename -h -d z2_soil,sdepth -v z2_soil,sdepth tmp_GCday_${year}_mrsos.nc

ncks -h -O -v soiice  $exp.daily.$year.nc  tmp_GCday_${year}_soiice.nc
ncrename -h -d z2_soil,sdepth -v z2_soil,sdepth tmp_GCday_${year}_soiice.nc

# mrso: total soil moisture content, summed over all soil layers
cdo vertsum tmp_GCday_${year}_mrsos.nc tmp_GCday_${year}_mrso.nc
ncrename -h -O -v mrsos,mrso tmp_GCday_${year}_mrso.nc tmp_GCday_${year}_mrso.nc
ncatted -h -a name,mrso,o,c,"mrso" tmp_GCday_${year}_mrso.nc
ncatted -h -a long_name,mrso,o,c,"Total Soil moisture content" tmp_GCday_${year}_mrso.nc
ncatted -h -a standard_name,mrso,o,c,"soil_moisture_content" \
    tmp_GCday_${year}_mrso.nc mrso_daily_ACCESS_${expname}_1_${year}.nc

#mrfso: mass of frozen water, summed over all layers
cdo mul tmp_GCday_${year}_mrsos.nc tmp_GCday_${year}_soiice.nc tmp_GCday_${year}_mrfso_lev.nc
cdo vertsum tmp_GCday_${year}_mrfso_lev.nc tmp_GCday_${year}_mrfso.nc
ncrename -h -O -v mrsos,mrfso tmp_GCday_${year}_mrfso.nc tmp_GCday_${year}_mrfso.nc
ncatted -h -a name,mrfso,o,c,"mrfso" tmp_GCday_${year}_mrfso.nc
ncatted -h -a long_name,mrfso,o,c,"Soil Frozen Water Content" tmp_GCday_${year}_mrfso.nc
ncatted -h -a standard_name,mrfso,o,c,"soil_frozen_water_content" \
    tmp_GCday_${year}_mrfso.nc mrfso_daily_ACCESS_${expname}_1_${year}.nc

#mrlsl: water content of soil layer per layer
ncrename -h -O -v mrsos,mrlsl tmp_GCday_${year}_mrsos.nc tmp_GCday_${year}_mrlsl.nc
ncatted -h -a name,mrlsl,o,c,"mrlsl" tmp_GCday_${year}_mrlsl.nc
ncatted -h -a long_name,mrlsl,o,c,"Water content of Soil Layer" tmp_GCday_${year}_mrlsl.nc
ncatted -h -a standard_name,mrlsl,o,c,"moisture_content_of_soil_layer" \
    tmp_GCday_${year}_mrlsl.nc mrlsl_daily_ACCESS_${expname}_1_${year}.nc

# mrsos: Moisture in upper portion of soil column, inegrate over uppermost 10cm -> first two layers plus 0.12987*lev3
ncks -h -F -d sdepth,1,2 tmp_GCday_${year}_mrsos.nc tmp_GCday_${year}_mrsos_lev1_2.nc
ncks -h -F -d sdepth,3 tmp_GCday_${year}_mrsos.nc tmp_GCday_${year}_mrsos_lev3.nc
cdo mulc,0.12987 tmp_GCday_${year}_mrsos_lev3.nc tmp_GCday_${year}_mrsos_0.02m.nc
cdo vertsum tmp_GCday_${year}_mrsos_lev1_2.nc tmp_GCday_${year}_mrsos_vertsum.nc
cdo add tmp_GCday_${year}_mrsos_vertsum.nc tmp_GCday_${year}_mrsos_0.02m.nc tmp_GCday_${year}_mrsos_sum.nc
ncatted -h -a long_name,mrsos,o,c,"Moisture in Upper Portion of Soil Column" \
    tmp_GCday_${year}_mrsos_sum.nc
ncatted -h -a standard_name,mrsos,o,c,"moisture content of soil layer" \
    tmp_GCday_${year}_mrsos_sum.nc mrsos_daily_ACCESS_${expname}_1_${year}.nc

#mrros: surface runoff
ncks -h -O -v mrros $exp.daily.$year.nc tmp_GCday_${year}_mrros.nc
ncatted -h -a long_name,mrros,o,c,"Surface Runoff" tmp_GCday_${year}_mrros.nc \
    mrros_daily_ACCESS_${expname}_1_${year}.nc

#mrro: total runoff, surface + subsurface runoff
ncks -h -O -v smrros $exp.daily.$year.nc tmp_GCday_${year}_smrros.nc

cdo add mrros_daily_ACCESS_${expname}_1_${year}.nc tmp_GCday_${year}_smrros.nc tmp_GCday_${year}_mrro.nc
ncrename -h -O -v mrros,mrro tmp_GCday_${year}_mrro.nc tmp_GCday_${year}_mrro.nc
ncatted -h -a name,mrro,o,c,"mrro" tmp_GCday_${year}_mrro.nc
ncatted -h -a long_name,mrro,o,c,"Total Runoff" tmp_GCday_${year}_mrro.nc
ncatted -h -a standard_name,mrro,o,c,"runoff_flux" tmp_GCday_${year}_mrro.nc mrro_daily_ACCESS_${expname}_1_${year}.nc

#clean up
rm tmp_*

#evspsblveg: evaporation from canopy
ncks -h -O -v evpcan $exp.daily.$year.nc tmp_GCday_${year}_evpcan.nc
ncrename -h -O -v evpcan,evspsblveg tmp_GCday_${year}_evpcan.nc tmp_GCday_${year}_evspsblveg.nc
ncatted -h -a name,evspsblveg,o,c,"evspsblveg" tmp_GCday_${year}_evspsblveg.nc
ncatted -h -a long_name,evspsblveg,o,c,"Evaporation from Canopy" tmp_GCday_${year}_evspsblveg.nc
ncatted -h -a standard_name,evspsblveg,o,c,"water_evaporation_flux_from_canopy" tmp_GCday_${year}_evspsblveg.nc \
    evspsblveg_daily_ACCESS_${expname}_1_${year}.nc

#evspsblsoi: evaporation from soil
ncks -h -O -v evpsoil $exp.daily.$year.nc tmp_GCday_${year}_evpsoil.nc
ncrename -h -O -v evpsoil,evspsblsoi tmp_GCday_${year}_evpsoil.nc tmp_GCday_${year}_evspsblsoi.nc
ncatted -h -a name,evspsblsoi,o,c,"evspsblsoi" tmp_GCday_${year}_evspsblsoi.nc
ncatted -h -a long_name,evspsblsoi,o,c,"Water Evaporation from Soil" tmp_GCday_${year}_evspsblsoi.nc
ncatted -h -a standard_name,evspsblsoi,o,c,"water_evaporation_flux_from_soil" tmp_GCday_${year}_evspsblsoi.nc \
    evspsblsoi_daily_ACCESS_${expname}_1_${year}.nc

#tran: transpiration, ETtot-ETsoil-ETveg
#ETtot: latent heat flux in kg m-2 s-1 -> hfls first
ncks -h -O -v hfls $exp.daily.$year.nc tmp_GCday_${year}_hfls.nc
ncatted -h -a long_name,hfls,o,c,"Surface Upward Latent Heat Flux" tmp_GCday_${year}_hfls.nc \
    hfls_daily_ACCESS_${expname}_1_${year}.nc

cdo divc,2500000 tmp_GCday_${year}_hfls.nc tmp_GCday_${year}_ettmp.nc
cdo mulc,86400 tmp_GCday_${year}_ettmp.nc tmp_GCday_${year}_ettot.nc
ncrename -h -O -v hfls,ettot tmp_GCday_${year}_ettot.nc
ncatted -h -a units,ettot,o,c,"kg m-2 s-1" tmp_GCday_${year}_ettot.nc
# evspsblveg and evspsblsoi only for land surface calculated in kg m-2 s-1
# -> need to take land fraction into account, but keep missing values over ocean
if [ $year == $year_start ]; then
cdo setctomiss,0 /home/561/rzl561/input_data/qrparm.landfrac.nc landfrac.nc
cdo gtc,0.7 landfrac.nc landfrac_gt07.nc
cdo setctomiss,0 landfrac_gt07.nc landfrac_gt07_mask.nc
fi
cdo mul tmp_GCday_${year}_evspsblsoi.nc landfrac.nc \
    tmp_GCday_${year}_evspsblsoi_landfrac.nc
cdo mul tmp_GCday_${year}_evspsblveg.nc landfrac.nc \
    tmp_GCday_${year}_evspsblveg_landfrac.nc

cdo sub tmp_GCday_${year}_ettot.nc tmp_GCday_${year}_evspsblsoi_landfrac.nc tmp_GCday_${year}_ettot-etsoi.nc
cdo sub tmp_GCday_${year}_ettot-etsoi.nc tmp_GCday_${year}_evspsblveg_landfrac.nc tmp_GCday_${year}_tran.nc

cdo gtc,0 tmp_GCday_${year}_tran.nc tmp_GCday_${year}_tran_posmask.nc
cdo setctomiss,0 tmp_GCday_${year}_tran_posmask.nc tmp_GCday_${year}_tran_posmaskmiss.nc
cdo mul tmp_GCday_${year}_tran.nc tmp_GCday_${year}_tran_posmaskmiss.nc tmp_GCday_${year}_tran_noneg.nc 
cdo mul tmp_GCday_${year}_tran_noneg.nc landfrac_gt07_mask.nc tmp_GCday_${year}_tranmaskcoast.nc 

ncrename -h -O -v ettot,tran tmp_GCday_${year}_tranmaskcoast.nc tmp_GCday_${year}_tranmaskcoast.nc
ncatted -h -a name,tran,o,c,"tran" tmp_GCday_${year}_tranmaskcoast.nc
ncatted -h -a units,tran,o,c,"kg m-2 s-1" tmp_GCday_${year}_tranmaskcoast.nc
ncatted -h -a long_name,tran,o,c,"Transpiration" tmp_GCday_${year}_tranmaskcoast.nc
ncatted -h -a standard_name,tran,o,c,"transpiration_flux" tmp_GCday_${year}_tranmaskcoast.nc
ncatted -h -O -a comment,tran,c,c,"calculated from hfls-evspsblsoi-evspsblveg,\n evspsblsoi and evspsblveg defined only over land, scaled by land fraction\n but very high values of tran along coast not realistic, due to grid point average including ocean points->gridpoints with less than 70%land are set to NaN, values below zero are set to NaN" \
    tmp_GCday_${year}_tranmaskcoast.nc tran_daily_ACCESS_${expname}_1_${year}.nc

#clean up
rm tmp_*

#tsl: soil temperature
ncks -h -O -v tsl $exp.daily.$year.nc tmp_GCday_${year}_tsl.nc
ncatted -h -a long_name,tsl,o,c,"Temperature of Soil" tmp_GCday_${year}_tsl.nc \
    tsl_daily_ACCESS_${expname}_1_${year}.nc

#huss: near-surface specific humidity
ncks -h -O -v huss $exp.daily.$year.nc tmp_GCday_${year}_huss.nc
ncatted -h -a long_name,huss,o,c,"Near-Surface Specific Humidity" tmp_GCday_${year}_huss.nc
ncatted -h -O -a comment,huss,c,c,"reported at 1.5m above surface" tmp_GCday_${year}_huss.nc \
    huss_daily_ACCESS_${expname}_1_${year}.nc

#tasmin
ncks -h -O -v tasmin $exp.daily.$year.nc tmp_GCday_${year}_tasmin.nc
ncatted -h -a long_name,tasmin,o,c,"Daily Maximum Near-Surface Air Temperature" tmp_GCday_${year}_tasmin.nc
ncatted -h -O -a comment,tasmin,c,c,"reported at 1.5m above surface" tmp_GCday_${year}_tasmin.nc \
    tasmin_daily_ACCESS_${expname}_1_${year}.nc
#tasmin for monthly
cdo monmean tasmin_daily_ACCESS_${expname}_1_${year}.nc tasmin_monmean_ACCESS_${expname}_1_${year}.nc

#tasmax
ncks -h -O -v tasmax $exp.daily.$year.nc tmp_GCday_${year}_tasmax.nc
ncatted -h -a long_name,tasmax,o,c,"Daily Maximum Near-Surface Air Temperature" tmp_GCday_${year}_tasmax.nc
ncatted -h -O -a comment,tasmax,c,c,"reported at 1.5m above surface" tmp_GCday_${year}_tasmax.nc \
    tasmax_daily_ACCESS_${expname}_1_${year}.nc
#tasmax for monthly
cdo monmean tasmax_daily_ACCESS_${expname}_1_${year}.nc tasmax_monmean_ACCESS_${expname}_1_${year}.nc

#tas: near-surface air temperature
ncks -h -O -v tas $exp.daily.$year.nc tmp_GCday_${year}_tas.nc
ncatted -h -a long_name,tas,o,c,"Near-Surface Air Temperature" tmp_GCday_${year}_tas.nc
ncatted -h -O -a comment,tas,c,c,"reported at 1.5m above surface" tmp_GCday_${year}_tas.nc \
    tas_daily_ACCESS_${expname}_1_${year}.nc

#pr: precipitation
ncks -h -O -v pr $exp.daily.$year.nc tmp_GCday_${year}_pr.nc
ncatted -h -a long_name,pr,o,c,"Precipitation" tmp_GCday_${year}_pr.nc \
    pr_daily_ACCESS_${expname}_1_${year}.nc

#psl: sea level pressure
ncks -h -O -v psl $exp.daily.$year.nc tmp_GCday_${year}_psl.nc
ncatted -h -a long_name,psl,o,c,"Sea Level Pressure" tmp_GCday_${year}_psl.nc \
    psl_daily_ACCESS_${expname}_1_${year}.nc

#clean up
rm tmp_*

#sfcWind: wind speed at surface
ncks -h -O -v wnd $exp.daily.$year.nc tmp_GCday_${year}_sfcWind.nc
ncrename -h -O -v wnd,sfcWind tmp_GCday_${year}_sfcWind.nc
ncatted -h -a name,sfcWind,o,c,"sfcWind" tmp_GCday_${year}_sfcWind.nc
ncatted -h -a long_name,sfcWind,o,c,"Near Surface Wind Speed" tmp_GCday_${year}_sfcWind.nc
ncatted -h -O -a comment,sfcWind,o,c,"reported at 10m above surface on B grid" tmp_GCday_${year}_sfcWind.nc \
    sfcWind_daily_ACCESS_${expname}_1_${year}.nc

#rhs: near-surface relative humidity
ncks -h -O -v hurs $exp.daily.$year.nc tmp_GCday_${year}_hurs.nc
ncrename -h -O -v hurs,rhs tmp_GCday_${year}_hurs.nc tmp_GCday_${year}_rhs.nc
ncatted -h -a name,rhs,o,c,"rhs" tmp_GCday_${year}_rhs.nc
ncatted -h -a long_name,rhs,o,c,"Near-Surface Relative Humidity" tmp_GCday_${year}_rhs.nc
ncatted -h -O -a comment,rhs,c,c,"reported at 1.5m above surface" tmp_GCday_${year}_rhs.nc \
    rhs_daily_ACCESS_${expname}_1_${year}.nc

#rhsmin: daily minimum near-surface relative humidity
ncks -h -O -v hursmin $exp.daily.$year.nc tmp_GCday_${year}_hursmin.nc
ncrename -h -O -v hursmin,rhsmin tmp_GCday_${year}_hursmin.nc tmp_GCday_${year}_rhsmin.nc
ncatted -h -a name,rhsmin,o,c,"rhsmin" tmp_GCday_${year}_rhsmin.nc
ncatted -h -a long_name,rhsmin,o,c,"Surface Daily Minimum Relative Humidity" tmp_GCday_${year}_rhsmin.nc
ncatted -h -O -a comment,rhsmin,c,c,"reported at 1.5m above surface" tmp_GCday_${year}_rhsmin.nc \
    rhsmin_daily_ACCESS_${expname}_1_${year}.nc
#rhsmin for monthly
cdo monmean rhsmin_daily_ACCESS_${expname}_1_${year}.nc rhsmin_monmean_ACCESS_${expname}_1_${year}.nc

#rhsmax: daily maximum near-surface relative humidity
ncks -h -O -v hursmax $exp.daily.$year.nc tmp_GCday_${year}_hursmax.nc
ncrename -h -O -v hursmax,rhsmax tmp_GCday_${year}_hursmax.nc tmp_GCday_${year}_rhsmax.nc
ncatted -h -a name,rhsmax,o,c,"rhsmax" tmp_GCday_${year}_rhsmax.nc
ncatted -h -a long_name,rhsmax,o,c,"Surface Daily Maximum Relative Humidity" tmp_GCday_${year}_rhsmax.nc
ncatted -h -O -a comment,rhsmax,c,c,"reported at 1.5m above surface" tmp_GCday_${year}_rhsmax.nc \
    rhsmax_daily_ACCESS_${expname}_1_${year}.nc
#rhsmax for monthly
cdo monmean rhsmax_daily_ACCESS_${expname}_1_${year}.nc rhsmax_monmean_ACCESS_${expname}_1_${year}.nc

#snc: surface snow area fraction -> not available, only snow amount
ncks -h -O -v snw_land $exp.daily.$year.nc tmp_GCday_${year}_snw_land.nc
ncatted -h -a long_name,snw_land,o,c,"Surface Snow Amount over Land" tmp_GCday_${year}_snw_land.nc \
    snw_land_daily_ACCESS_${expname}_1_${year}.nc

#clt: cloud fraction
ncks -h -O -v clt $exp.daily.$year.nc tmp_GCday_${year}_clt.nc
cdo mulc,100 tmp_GCday_${year}_clt.nc tmp_GCday_${year}_clt_prct.nc
ncatted -h -a units,clt,o,c,"%" tmp_GCday_${year}_clt_prct.nc  
ncatted -h -a long_name,clt,o,c,"Total Cloud Fraction" tmp_GCday_${year}_clt_prct.nc \
    clt_daily_ACCESS_${expname}_1_${year}.nc

#tslsi: Surface Temperature where land or sea ice
#need ts, ts_sea (sst from ancillary -> monthly), sic (from ancillary -> monthly)
ncks -h -O -v ts $exp.daily.$year.nc tmp_GCday_${year}_ts.nc
if  [ $year -le 2006 ]; then
    sst_file=/short/dt6/rzl561/input_data/sst_amip_daily_${year}.nc
else sst_file=/short/dt6/rzl561/input_data/ACCESS1-3_rcp85/tos_day_ACCESS1-3_rcp85_r1i1p1_${year}.nc
fi
if  [ $year -le 2006 ]; then
    sic_file=/short/dt6/rzl561/input_data/seaice_amip_daily_${year}.nc
else sic_file=/short/dt6/rzl561/input_data/ACCESS1-3_rcp85/sic_day_ACCESS1-3_rcp85_r1i1p1_${year}.nc
fi
#interpolate monthly to daily data
#land fraction: /home/561/rzl561/input_data/qrparm.landfrac.nc
#land or sea ice fraction
if [ $year == $year_start ]; then
cdo mulc,-1 /home/561/rzl561/input_data/qrparm.landfrac.nc neg_landfrac.nc
cdo addc,1 neg_landfrac.nc not_land.nc
fi
cdo mul not_land.nc $sic_file tmp_sea_ice_frac_${year}.nc
cdo add /home/561/rzl561/input_data/qrparm.landfrac.nc tmp_sea_ice_frac_${year}.nc tmp_land_sea_ice_${year}.nc
#open ocean fraction
cdo mulc,-1 $sic_file tmp_neg_sea_ice_frac_${year}.nc
cdo addc,1 tmp_neg_sea_ice_frac_${year}.nc tmp_not_sea_ice_${year}.nc
cdo mul not_land.nc tmp_not_sea_ice_${year}.nc tmp_open_ocean_frac_${year}.nc
cdo mul $sst_file tmp_open_ocean_frac_${year}.nc tmp_open_ocean_temp_${year}.nc

cdo sub tmp_GCday_${year}_ts.nc tmp_open_ocean_temp_${year}.nc tmp_ts-sst_${year}.nc
cdo div tmp_ts-sst_${year}.nc tmp_land_sea_ice_${year}.nc tmp_GCday_${year}_tslsi.nc
cdo ltc,360 tmp_GCday_${year}_tslsi.nc tslsi_mask_toohigh.nc
cdo setctomiss,0 tslsi_mask_toohigh.nc tslsi_mask_toohighmiss.nc
cdo gtc,170 tmp_GCday_${year}_tslsi.nc tslsi_mask_toolow.nc
cdo setctomiss,0 tslsi_mask_toolow.nc tslsi_mask_toolowmiss.nc

cdo mul tmp_GCday_${year}_tslsi.nc tslsi_mask_toohighmiss.nc tmp_GCday_${year}_tslsi_maskhigh.nc
cdo mul tmp_GCday_${year}_tslsi_maskhigh.nc tslsi_mask_toolowmiss.nc tmp_GCday_${year}_tslsi_mask.nc

ncrename -h -O -v ts,tslsi tmp_GCday_${year}_tslsi_mask.nc
ncatted -h -a name,tslsi,o,c,"tslsi" tmp_GCday_${year}_tslsi_mask.nc
ncatted -h -a long_name,tslsi,o,c,"Surface Temperature Where Land or Sea Ice" tmp_GCday_${year}_tslsi_mask.nc
ncatted -h -a standard_name,tslsi,o,c,"surface_temperature" tmp_GCday_${year}_tslsi_mask.nc
ncatted -h -O -a comment,tslsi,c,c,"tslsi=(ts-ts_sea*A_o)/A_lsi)\n ts=global skin temperature\n ts_sea=sea surface temperature from amip ancillary file, linear  interpolated from monthly to daily values until 2006, then daily output from CMIP5\n A_o=(1-A_l)*(1-sic)) (open ocean fraction)/n A_l=land fraction\n sic=sea ice fraction from amip ancillary file, linear interpolated from monthly to daily values until 2006, then daily output from CMIP5\n A_lsi=A_l+(1-A_l)*sic (land or sea ice fraction). Unreasonably low and high values set to NaN" tmp_GCday_${year}_tslsi_mask.nc \
    tslsi_daily_ACCESS_${expname}_1_${year}.nc

#prc: convective precipitation, prc1+prc2
ncks -h -O -v prc1 $exp.daily.$year.nc tmp_GCday_${year}_prc1.nc
ncks -h -O -v prc2 $exp.daily.$year.nc tmp_GCday_${year}_prc2.nc
cdo add tmp_GCday_${year}_prc1.nc tmp_GCday_${year}_prc2.nc tmp_GCday_${year}_prc.nc
ncrename -h -O -v prc1,prc tmp_GCday_${year}_prc.nc
ncatted -h -a name,prc,o,c,"prc" tmp_GCday_${year}_prc.nc
ncatted -h -a long_name,prc,o,c,"Convective Precipitation" tmp_GCday_${year}_prc.nc
ncatted -h -a standard_name,prc,o,c,"convective_precipitation_flux" tmp_GCday_${year}_prc.nc \
    prc_daily_ACCESS_${expname}_1_${year}.nc

#clean up
rm tmp_*

#hfss: sensible heat flux
ncks -h -O -v hfss $exp.daily.$year.nc tmp_GCday_${year}_hfss.nc
ncatted -h -a long_name,hfss,o,c,"Surface Upward Sensible Heat Flux" tmp_GCday_${year}_hfss.nc \
    hfss_daily_ACCESS_${expname}_1_${year}.nc

#rlds: surface down longwave
ncks -h -O -v rlds $exp.daily.$year.nc tmp_GCday_${year}_rlds.nc
ncatted -h -a long_name,rlds,o,c,"Surface Downwelling Longwave Radiation" tmp_GCday_${year}_rlds.nc
ncatted -h -a standard_name,rlds,o,c,"surface_downwelling_longwave_flux_in_air" \
    tmp_GCday_${year}_rlds.nc rlds_daily_ACCESS_${expname}_1_${year}.nc

#rlus: surface upwelling longwave, LWdown-LWnet
#LWnet:
ncks -h -O -v rls $exp.daily.$year.nc tmp_GCday_${year}_rls.nc
#LWup=LWdown-LWnet
cdo sub tmp_GCday_${year}_rlds.nc tmp_GCday_${year}_rls.nc tmp_GCday_${year}_rlus.nc
ncrename -h -O -v rlds,rlus tmp_GCday_${year}_rlus.nc
ncatted -h -a name,rlus,o,c,"rlus" tmp_GCday_${year}_rlus.nc
ncatted -h -a long_name,rlus,o,c,"Surface Upwelling Longwave Radiation" tmp_GCday_${year}_rlus.nc
ncatted -h -a standard_name,rlus,o,c,"surface_upwelling_longwave_flux_in_air" \
    tmp_GCday_${year}_rlus.nc rlus_daily_ACCESS_${expname}_1_${year}.nc

#rsds: surface downwelling shortwave
ncks -O -v rsds $exp.daily.$year.nc tmp_GCday_${year}_rsds.nc
ncatted -h -a long_name,rsds,o,c,"Surface Downwelling Shortwave Radiation" tmp_GCday_${year}_rsds.nc
ncatted -h -a standard_name,rsds,o,c,"surface_downwelling_shortwave_flux_in_air" \
    tmp_GCday_${year}_rsds.nc rsds_daily_ACCESS_${expname}_1_${year}.nc

#rsus: surface upwelling shortwave, SWdown-SWnet
#SWnet
ncks -h -O -v rss $exp.daily.$year.nc tmp_GCday_${year}_rss.nc
#SWup=SWdown-SWnet
cdo sub tmp_GCday_${year}_rsds.nc tmp_GCday_${year}_rss.nc tmp_GCday_${year}_rsus.nc
ncrename -h -O -v rsds,rsus tmp_GCday_${year}_rsus.nc
ncatted -h -a name,rsus,o,c,"rsus" tmp_GCday_${year}_rsus.nc
ncatted -h -a long_name,rsus,o,c,"Surface Upwelling Shortwave Radiation" tmp_GCday_${year}_rsus.nc
ncatted -h -a standard_name,rsus,o,c,"surface_upwelling_shortwave_flux_in_air" \
    tmp_GCday_${year}_rsus.nc rsus_daily_ACCESS_${expname}_1_${year}.nc

#clean up
rm tmp_*

let year=year+1
done #year

#qsub -V -v GCday_formatting_timeseries.sh
