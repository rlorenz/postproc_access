#!/bin/bash
## \file GCmon_formatting.sh
#  \author Ruth Lorenz (r.lorenz@unsw.edu.au)
#  \brief put monthly GLACE output in correct format, output is archived on mdss
#  \usage qsub -V -v -lother=mdss GCmon_tran_tslsi_only.sh

#PBS -q copyq
#PBS -N GCmon_tran
#PBS -j oe
#PBS -l other=mdss
#PBS -l ncpus=1
###PBS -l mem=1536mb
#PBS -l walltime=04:00:00
#PBS -l wd

##-----------------------##
## load required modules ##
##-----------------------##

module load netcdf
module load nco
module load cdo
 
##---------------------##
## user specifications ##
##-------------------- ##

exp=uamoa
year_start=1950
year_end=2100
archive=$USER/$exp/
workdir=/short/dt6/$USER/postproc/$exp/work/monthly/
outdir=/short/dt6/$USER/postproc/$exp/timeseries/GCmon/

#determine final experiment name
if [ $exp == "uamoa" ]; then
    expname=CTL
elif [ $exp == "uamoh" ]; then
    expname=GC1A85
elif [ $exp == "uamoj" ]; then
    expname=GC1B85
elif [ $exp == "uamol" ]; then
    expname=GC1C85
fi

##---------------------##

mkdir /short/dt6/$USER/postproc
mkdir /short/dt6/$USER/postproc/$exp
mkdir /short/dt6/$USER/postproc/$exp/timeseries/
mkdir ${outdir}
mkdir ${workdir}

cd ${workdir}
##-------------------------------------------##
## monthly files:                            ##
##-------------------------------------------##

## get data from mdss, loop over year, unzip
## and concatenate yearly archives 

year=2090

while [  $year -le $year_end ]; do
#loop over years
    echo "Processing year $year"
       
    for month in 01 02 03 04 05 06 07 08 09 10 11 12
    do
	#monthly files, check availability first
	file_mon=`mdss dmls -l ${archive}/$exp.monthly.$year-$month-00T00.nc.gz | awk '{print $2}'`
	if [[ "$file_mon" != 1 ]]; then
	    echo "ERROR: missing file $exp.monthly.$year-$month-00T00.nc.gz in ${archive}" 1>&2
	    #exit 1
	    fi

	mdss get ${archive}/$exp.monthly.$year-$month-00T00.nc.gz
	gunzip $exp.monthly.$year-$month-00T00.nc.gz
	done #month

# concatenate yearly archives
ncrcat -O $exp.monthly.$year-??-00T00.nc ${workdir}/$exp.monthly.$year.nc

# clean up
rm $exp.monthly.????-??-00T00.nc

#bring variables in correct format and store one file per variable
ncatted -h -a long_name,lon,o,c,"longitude" $exp.monthly.$year.nc
ncatted -h -a long_name,lat,o,c,"latitude" $exp.monthly.$year.nc

ncatted -h -O -a _FillValue,,o,f,1.0e20 $exp.monthly.$year.nc
ncatted -h -O -a missing_value,,o,f,1.0e20 $exp.monthly.$year.nc

#evspsblveg: evaporation from canopy
ncks -O -v evpcan $exp.monthly.$year.nc tmp_GCmon_${year}_evpcan.nc
ncrename -h -O -v evpcan,evspsblveg tmp_GCmon_${year}_evpcan.nc tmp_GCmon_${year}_evspsblveg.nc
ncatted -h -a name,evspsblveg,o,c,"evspsblveg" tmp_GCmon_${year}_evspsblveg.nc
ncatted -h -a long_name,evspsblveg,o,c,"Evaporation from Canopy" tmp_GCmon_${year}_evspsblveg.nc
ncatted -h -a standard_name,evspsblveg,o,c,"water_evaporation_flux_from_canopy" tmp_GCmon_${year}_evspsblveg.nc \
    evspsblveg_monthly_ACCESS_${expname}_1_${year}.nc

#evspsblsoi: evaporation from soil
ncks -O -v evpsoil $exp.monthly.$year.nc tmp_GCmon_${year}_evpsoil.nc
ncrename -O -v evpsoil,evspsblsoi tmp_GCmon_${year}_evpsoil.nc tmp_GCmon_${year}_evspsblsoi.nc
ncatted -h -a name,evspsblsoi,o,c,"evspsblsoi" tmp_GCmon_${year}_evspsblsoi.nc
ncatted -h -a long_name,evspsblsoi,o,c,"Water Evaporation from Soil" tmp_GCmon_${year}_evspsblsoi.nc
ncatted -h -a standard_name,evspsblsoi,o,c,"water_evaporation_flux_from_soil" tmp_GCmon_${year}_evspsblsoi.nc \
    evspsblsoi_monthly_ACCESS_${expname}_1_${year}.nc

#tran: transpiration, ETtot-ETsoil-ETveg
#ETtot: latent heat flux in kg m-2 s-1 -> hfls first
ncks -O -v hfls $exp.monthly.$year.nc tmp_GCmon_${year}_hfls.nc
ncatted -h -a long_name,hfls,o,c,"Surface Upward Latent Heat Flux" tmp_GCmon_${year}_hfls.nc \
    hfls_monthly_ACCESS_${expname}_1_${year}.nc

cdo divc,2500000 tmp_GCmon_${year}_hfls.nc tmp_GCmon_${year}_ettmp.nc
cdo mulc,86400 tmp_GCmon_${year}_ettmp.nc tmp_GCmon_${year}_ettot.nc
ncrename -h -O -v hfls,ettot tmp_GCmon_${year}_ettot.nc
ncatted -h -a units,ettot,o,c,"kg m-2 s-1" tmp_GCmon_${year}_ettot.nc
# evspsblveg and evspsblsoi only for land surface calculated in kg m-2 s-1
# -> need to take land fraction into account, but keep missing values over ocean
if [ $year == $year_start ]; then
cdo setctomiss,0 /home/561/rzl561/input_data/qrparm.landfrac.nc landfrac.nc
cdo gtc,0.7 landfrac.nc landfrac_gt07.nc
cdo setctomiss,0 landfrac_gt07.nc landfrac_gt07_mask.nc
fi

cdo mul tmp_GCmon_${year}_evspsblsoi.nc landfrac.nc \
    tmp_GCmon_${year}_evspsblsoi_landfrac.nc
cdo mul tmp_GCmon_${year}_evspsblveg.nc landfrac.nc \
    tmp_GCmon_${year}_evspsblveg_landfrac.nc

cdo sub tmp_GCmon_${year}_ettot.nc tmp_GCmon_${year}_evspsblsoi_landfrac.nc \
    tmp_GCmon_${year}_ettot-etsoi.nc
cdo sub tmp_GCmon_${year}_ettot-etsoi.nc tmp_GCmon_${year}_evspsblveg_landfrac.nc tmp_GCmon_${year}_tran.nc

cdo gtc,0 tmp_GCmon_${year}_tran.nc tmp_GCmon_${year}_tran_posmask.nc
cdo setctomiss,0 tmp_GCmon_${year}_tran_posmask.nc tmp_GCmon_${year}_tran_posmaskmiss.nc
cdo mul tmp_GCmon_${year}_tran.nc tmp_GCmon_${year}_tran_posmaskmiss.nc tmp_GCmon_${year}_tran_noneg.nc
cdo mul tmp_GCmon_${year}_tran_noneg.nc landfrac_gt07_mask.nc tmp_GCmon_${year}_tranmaskcoast.nc 

ncrename -h -O -v ettot,tran tmp_GCmon_${year}_tranmaskcoast.nc tmp_GCmon_${year}_tranmaskcoast.nc
ncatted -h -a name,tran,o,c,"tran" tmp_GCmon_${year}_tranmaskcoast.nc
ncatted -h -a units,tran,o,c,"kg m-2 s-1" tmp_GCmon_${year}_tranmaskcoast.nc
ncatted -h -a long_name,tran,o,c,"Transpiration" tmp_GCmon_${year}_tranmaskcoast.nc
ncatted -h -a standard_name,tran,o,c,"transpiration_flux" tmp_GCmon_${year}_tranmaskcoast.nc
ncatted -h -O -a comment,tran,c,c,"calculated from hfls-evspsblsoi-evspsblveg,\n evspsblsoi and evspsblveg defined only over land, scaled by land fraction\n but very high values of tran along coast not realistic, due to grid point average including ocean points->gridpoints with less than 70%land are set to NaN, values below zero are set to NaN" tmp_GCmon_${year}_tranmaskcoast.nc tran_monthly_ACCESS_${expname}_1_${year}.nc

#tslsi: Surface Temperature where land or sea ice
#need ts, ts_sea (sst from ancillary), sic (from ancillary)
ncks -O -v ts $exp.monthly.$year.nc tmp_GCmon_${year}_ts.nc
if  [ $year -le 2006 ]; then
    sst_file=/short/dt6/rzl561/input_data/sst_amip_${year}.nc
else sst_file=/short/dt6/rzl561/input_data/ACCESS1-3_rcp85/tos_Omon_ACCESS1-3_rcp85_r1i1p1_${year}.nc
fi
if  [ $year -le 2006 ]; then
    sic_file=/short/dt6/rzl561/input_data/seaice_amip_${year}.nc
    sic_var=iceconc
else sic_file=/short/dt6/rzl561/input_data/ACCESS1-3_rcp85/sic_OImon_ACCESS1-3_rcp85_r1i1p1_${year}.nc
    sic_var=sic
fi
#land fraction: /home/561/rzl561/input_data/qrparm.landfrac.nc
#land or sea ice fraction
if [ $year == $year_start ]; then
cdo mulc,-1 /home/561/rzl561/input_data/qrparm.landfrac.nc neg_landfrac.nc
cdo addc,1 neg_landfrac.nc not_land.nc
fi
cdo mul not_land.nc $sic_file tmp_sea_ice_frac_${year}.nc
cdo add /home/561/rzl561/input_data/qrparm.landfrac.nc tmp_sea_ice_frac_${year}.nc tmp_land_sea_ice_${year}.nc
#open ocean fraction
cdo mulc,-1 $sic_file tmp_neg_sea_ice_frac_${year}.nc
cdo addc,1 tmp_neg_sea_ice_frac_${year}.nc tmp_not_sea_ice_${year}.nc
cdo mul not_land.nc tmp_not_sea_ice_${year}.nc tmp_open_ocean_frac_${year}.nc
cdo mul $sst_file tmp_open_ocean_frac_${year}.nc tmp_open_ocean_temp_${year}.nc

cdo sub tmp_GCmon_${year}_ts.nc tmp_open_ocean_temp_${year}.nc tmp_ts-sst_${year}.nc
cdo div tmp_ts-sst_${year}.nc tmp_land_sea_ice_${year}.nc tmp_GCmon_${year}_tslsi.nc
cdo ltc,360 tmp_GCmon_${year}_tslsi.nc tslsi_mask_toohigh.nc
cdo setctomiss,0 tslsi_mask_toohigh.nc tslsi_mask_toohighmiss.nc
cdo gtc,170 tmp_GCmon_${year}_tslsi.nc tslsi_mask_toolow.nc
cdo setctomiss,0 tslsi_mask_toolow.nc tslsi_mask_toolowmiss.nc

cdo mul tmp_GCmon_${year}_tslsi.nc tslsi_mask_toohighmiss.nc tmp_GCmon_${year}_tslsi_maskhigh.nc
cdo mul tmp_GCmon_${year}_tslsi_maskhigh.nc tslsi_mask_toolowmiss.nc tmp_GCmon_${year}_tslsi_mask.nc

ncrename -O -v ts,tslsi tmp_GCmon_${year}_tslsi_mask.nc
ncatted -h -a name,tslsi,o,c,"tslsi" tmp_GCmon_${year}_tslsi_mask.nc
ncatted -h -a long_name,tslsi,o,c,"Surface Temperature Where Land or Sea Ice" tmp_GCmon_${year}_tslsi_mask.nc
ncatted -h -a standard_name,tslsi,o,c,"surface_temperature" tmp_GCmon_${year}_tslsi_mask.nc
ncatted -h -O -a comment,tslsi,c,c,"tslsi=(ts-ts_sea*A_o)/A_lsi)\n ts=gloabl skin temperature\n ts_sea=sea surface temperature from ancillary file\n A_o=(1-A_l)*(1-sic)) (open ocean fraction)/n A_l=land fraction\n sic=sea ice fraction from ancillary file\n A_lsi=A_l+(1-A_l)*sic (land or sea ice fraction). Unreasonably low and high values set to NaN" \
 tmp_GCmon_${year}_tslsi_mask.nc tslsi_monthly_ACCESS_${expname}_1_${year}.nc

#clean up
rm tmp_*

let year=year+1
done #year

ncrcat -O tran_monthly_ACCESS_${expname}_1_????.nc tran_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip tran_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv tran_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O tslsi_monthly_ACCESS_${expname}_1_????.nc tslsi_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip tslsi_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv tslsi_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

#clean up
rm ${workdir}/*
