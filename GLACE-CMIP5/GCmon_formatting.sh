#!/bin/bash
## \file GCmon_formatting.sh
#  \author Ruth Lorenz (r.lorenz@unsw.edu.au)
#  \brief put monthly GLACE output in correct format, output is archived on mdss
#  \usage qsub -V -v -lother=mdss GCmon_formatting.sh

#PBS -q copyq
#PBS -N GCmon_format
#PBS -j oe
#PBS -l other=mdss
#PBS -l ncpus=1
###PBS -l mem=1536mb
#PBS -l walltime=06:00:00
#PBS -l wd

##-----------------------##
## load required modules ##
##-----------------------##

module load netcdf
module load nco
module load cdo
 
##---------------------##
## user specifications ##
##-------------------- ##

exp=uamon
year_start=1950
year_end=2100
archive=$USER/$exp/
workdir=/short/dt6/$USER/postproc/$exp/work/monthly/
outdir=/short/dt6/$USER/postproc/$exp/timeseries/GCmon/

#determine final experiment name
if [ $exp == "uamoa" ]; then
    expname=CTL
elif [ $exp == "uamom" ]; then
    expname=GC1A85
elif [ $exp == "uamon" ] || [ $exp == "uamoj" ]; then
    expname=GC1B85
elif [ $exp == "uamol" ]; then
    expname=GC1C85
fi

##---------------------##

mkdir -p ${outdir}
mkdir -p ${workdir}

cd ${workdir}

##-------------------------------------------##
## monthly files:                            ##
##-------------------------------------------##

## get data from mdss, loop over year, unzip
## and concatenate yearly archives 

year=$year_start

while [  $year -le $year_end ]; do
#loop over years
    echo "Processing year $year"
       
    for month in 01 02 03 04 05 06 07 08 09 10 11 12
    do
	#monthly files, check availability first
	file_mon=`mdss dmls -l ${archive}/$exp.monthly.$year-$month-00T00.nc.gz | awk '{print $2}'`
	if [[ "$file_mon" != 1 ]]; then
	    echo "ERROR: missing file $exp.monthly.$year-$month-00T00.nc.gz in ${archive}" 1>&2
	    exit 1
	    fi

	mdss get ${archive}/$exp.monthly.$year-$month-00T00.nc.gz
	gunzip $exp.monthly.$year-$month-00T00.nc.gz
	done #month

# concatenate yearly archives
ncrcat -h -O $exp.monthly.$year-??-00T00.nc ${workdir}/$exp.monthly.$year.nc

# clean up
rm $exp.monthly.????-??-00T00.nc

#bring variables in correct format and store one file per variable
ncatted -h -a long_name,lon,o,c,"longitude" $exp.monthly.$year.nc
ncatted -h -a long_name,lat,o,c,"latitude" $exp.monthly.$year.nc

ncatted -h -O -a _FillValue,,o,f,1.0e20 $exp.monthly.$year.nc
ncatted -h -O -a _FillValue,time,d,, $exp.monthly.$year.nc

#Soil moisture: calculate mrsos, mrso, mrfso, mrlsl from original output (mrsos,soiice)
ncks -h -O -v mrsos $exp.monthly.$year.nc tmp_GCmon_${year}_mrsos.nc
ncrename -h -d z5_soil,sdepth -v z5_soil,sdepth tmp_GCmon_${year}_mrsos.nc

ncks -h -O -v soiice  $exp.monthly.$year.nc  tmp_GCmon_${year}_soiice.nc
ncrename -h -d z5_soil,sdepth -v z5_soil,sdepth tmp_GCmon_${year}_soiice.nc

# mrso: total soil moisture content, summed over all soil layers
cdo vertsum tmp_GCmon_${year}_mrsos.nc tmp_GCmon_${year}_mrso.nc
ncrename -h -O -v mrsos,mrso tmp_GCmon_${year}_mrso.nc tmp_GCmon_${year}_mrso.nc
ncatted -h -a name,mrso,o,c,"mrso" tmp_GCmon_${year}_mrso.nc
ncatted -h -a long_name,mrso,o,c,"Total Soil moisture content" tmp_GCmon_${year}_mrso.nc
ncatted -h -a standard_name,mrso,o,c,"soil_moisture_content" \
    tmp_GCmon_${year}_mrso.nc mrso_monthly_ACCESS_${expname}_1_${year}.nc

#mrfso: mass of frozen water, summed over all layers
cdo mul tmp_GCmon_${year}_mrsos.nc tmp_GCmon_${year}_soiice.nc tmp_GCmon_${year}_mrfso_lev.nc
cdo vertsum tmp_GCmon_${year}_mrfso_lev.nc tmp_GCmon_${year}_mrfso.nc
ncrename -h -O -v mrsos,mrfso tmp_GCmon_${year}_mrfso.nc tmp_GCmon_${year}_mrfso.nc
ncatted -h -a name,mrfso,o,c,"mrfso" tmp_GCmon_${year}_mrfso.nc
ncatted -h -a long_name,mrfso,o,c,"Soil Frozen Water Content" tmp_GCmon_${year}_mrfso.nc
ncatted -h -a standard_name,mrfso,o,c,"soil_frozen_water_content" \
    tmp_GCmon_${year}_mrfso.nc mrfso_monthly_ACCESS_${expname}_1_${year}.nc

#mrlsl: water content of soil layer per layer
ncrename -h -O -v mrsos,mrlsl tmp_GCmon_${year}_mrsos.nc tmp_GCmon_${year}_mrlsl.nc
ncatted -h -a name,mrlsl,o,c,"mrlsl" tmp_GCmon_${year}_mrlsl.nc
ncatted -h -a long_name,mrlsl,o,c,"Water content of Soil Layer" tmp_GCmon_${year}_mrlsl.nc
ncatted -h -a standard_name,mrlsl,o,c,"moisture_content_of_soil_layer" \
    tmp_GCmon_${year}_mrlsl.nc mrlsl_monthly_ACCESS_${expname}_1_${year}.nc

# mrsos: Moisture in upper portion of soil column, inegrate over uppermost 10cm -> first two layers plus 0.12987*lev3
ncks -F -d sdepth,1,2 tmp_GCmon_${year}_mrsos.nc tmp_GCmon_${year}_mrsos_lev1_2.nc
ncks -F -d sdepth,3 tmp_GCmon_${year}_mrsos.nc tmp_GCmon_${year}_mrsos_lev3.nc
cdo mulc,0.12987 tmp_GCmon_${year}_mrsos_lev3.nc tmp_GCmon_${year}_mrsos_0.02m.nc
cdo vertsum tmp_GCmon_${year}_mrsos_lev1_2.nc tmp_GCmon_${year}_mrsos_vertsum.nc
cdo add tmp_GCmon_${year}_mrsos_vertsum.nc tmp_GCmon_${year}_mrsos_0.02m.nc tmp_GCmon_${year}_mrsos_sum.nc
ncatted -h -a long_name,mrsos,o,c,"Moisture in Upper Portion of Soil Column" \
    tmp_GCmon_${year}_mrsos_sum.nc
ncatted -h -a standard_name,mrsos,o,c,"moisture content of soil layer" \
    tmp_GCmon_${year}_mrsos_sum.nc mrsos_monthly_ACCESS_${expname}_1_${year}.nc

#mrros: surface runoff
ncks -h -O -v mrros $exp.monthly.$year.nc tmp_GCmon_${year}_mrros.nc
ncatted -h -a long_name,mrros,o,c,"Surface Runoff" tmp_GCmon_${year}_mrros.nc \
    mrros_monthly_ACCESS_${expname}_1_${year}.nc

#mrro: total runoff, surface + subsurface runoff
ncks -h -O -v smrros $exp.monthly.$year.nc tmp_GCmon_${year}_smrros.nc

cdo add mrros_monthly_ACCESS_${expname}_1_${year}.nc tmp_GCmon_${year}_smrros.nc tmp_GCmon_${year}_mrro.nc
ncrename -O -v mrros,mrro tmp_GCmon_${year}_mrro.nc tmp_GCmon_${year}_mrro.nc
ncatted -h -a name,mrro,o,c,"mrro" tmp_GCmon_${year}_mrro.nc
ncatted -h -a long_name,mrro,o,c,"Total Runoff" tmp_GCmon_${year}_mrro.nc
ncatted -h -a standard_name,mrro,o,c,"runoff_flux" tmp_GCmon_${year}_mrro.nc mrro_monthly_ACCESS_${expname}_1_${year}.nc

#clean up
rm tmp_*

#evspsblveg: evaporation from canopy
ncks -h -O -v evpcan $exp.monthly.$year.nc tmp_GCmon_${year}_evpcan.nc
ncrename -h -O -v evpcan,evspsblveg tmp_GCmon_${year}_evpcan.nc tmp_GCmon_${year}_evspsblveg.nc
ncatted -h -a name,evspsblveg,o,c,"evspsblveg" tmp_GCmon_${year}_evspsblveg.nc
ncatted -h -a long_name,evspsblveg,o,c,"Evaporation from Canopy" tmp_GCmon_${year}_evspsblveg.nc
ncatted -h -a standard_name,evspsblveg,o,c,"water_evaporation_flux_from_canopy" tmp_GCmon_${year}_evspsblveg.nc \
    evspsblveg_monthly_ACCESS_${expname}_1_${year}.nc

#evspsblsoi: evaporation from soil
ncks -h -O -v evpsoil $exp.monthly.$year.nc tmp_GCmon_${year}_evpsoil.nc
ncrename -O -v evpsoil,evspsblsoi tmp_GCmon_${year}_evpsoil.nc tmp_GCmon_${year}_evspsblsoi.nc
ncatted -h -a name,evspsblsoi,o,c,"evspsblsoi" tmp_GCmon_${year}_evspsblsoi.nc
ncatted -h -a long_name,evspsblsoi,o,c,"Water Evaporation from Soil" tmp_GCmon_${year}_evspsblsoi.nc
ncatted -h -a standard_name,evspsblsoi,o,c,"water_evaporation_flux_from_soil" tmp_GCmon_${year}_evspsblsoi.nc \
    evspsblsoi_monthly_ACCESS_${expname}_1_${year}.nc

#tran: transpiration, ETtot-ETsoil-ETveg
#ETtot: latent heat flux in kg m-2 s-1 -> hfls first
ncks -h -O -v hfls $exp.monthly.$year.nc tmp_GCmon_${year}_hfls.nc
ncatted -h -a long_name,hfls,o,c,"Surface Upward Latent Heat Flux" tmp_GCmon_${year}_hfls.nc \
    hfls_monthly_ACCESS_${expname}_1_${year}.nc

cdo divc,2500000 tmp_GCmon_${year}_hfls.nc tmp_GCmon_${year}_ettmp.nc
cdo mulc,86400 tmp_GCmon_${year}_ettmp.nc tmp_GCmon_${year}_ettot.nc
ncrename -h -O -v hfls,ettot tmp_GCmon_${year}_ettot.nc
ncatted -h -a units,ettot,o,c,"kg m-2 s-1" tmp_GCmon_${year}_ettot.nc
# evspsblveg and evspsblsoi only for land surface calculated in kg m-2 s-1
# -> need to take land fraction into account, but keep missing values over ocean
if [ $year == $year_start ]; then
cdo setctomiss,0 /home/561/rzl561/input_data/qrparm.landfrac.nc landfrac.nc
cdo gtc,0.7 landfrac.nc landfrac_gt07.nc
cdo setctomiss,0 landfrac_gt07.nc landfrac_gt07_mask.nc
fi

cdo mul tmp_GCmon_${year}_evspsblsoi.nc landfrac.nc \
    tmp_GCmon_${year}_evspsblsoi_landfrac.nc
cdo mul tmp_GCmon_${year}_evspsblveg.nc landfrac.nc \
    tmp_GCmon_${year}_evspsblveg_landfrac.nc

cdo sub tmp_GCmon_${year}_ettot.nc tmp_GCmon_${year}_evspsblsoi_landfrac.nc \
    tmp_GCmon_${year}_ettot-etsoi.nc
cdo sub tmp_GCmon_${year}_ettot-etsoi.nc tmp_GCmon_${year}_evspsblveg_landfrac.nc tmp_GCmon_${year}_tran.nc

cdo gtc,0 tmp_GCmon_${year}_tran.nc tmp_GCmon_${year}_tran_posmask.nc
cdo setctomiss,0 tmp_GCmon_${year}_tran_posmask.nc tmp_GCmon_${year}_tran_posmaskmiss.nc
cdo mul tmp_GCmon_${year}_tran.nc tmp_GCmon_${year}_tran_posmaskmiss.nc tmp_GCmon_${year}_tran_noneg.nc 
cdo mul tmp_GCmon_${year}_tran_noneg.nc landfrac_gt07_mask.nc tmp_GCmon_${year}_tranmaskcoast.nc 

ncrename -h -O -v ettot,tran tmp_GCmon_${year}_tranmaskcoast.nc tmp_GCmon_${year}_tranmaskcoast.nc
ncatted -h -a name,tran,o,c,"tran" tmp_GCmon_${year}_tranmaskcoast.nc
ncatted -h -a units,tran,o,c,"kg m-2 s-1" tmp_GCmon_${year}_tranmaskcoast.nc
ncatted -h -a long_name,tran,o,c,"Transpiration" tmp_GCmon_${year}_tranmaskcoast.nc
ncatted -h -a standard_name,tran,o,c,"transpiration_flux" tmp_GCmon_${year}_tranmaskcoast.nc
ncatted -h -O -a comment,tran,c,c,"calculated from hfls-evspsblsoi-evspsblveg,\n evspsblsoi and evspsblveg defined only over land, scaled by land fraction\n but very high values of tran along coast not realistic, due to grid point average including ocean points->gridpoints with less than 70%land are set to NaN" tmp_GCmon_${year}_tranmaskcoast.nc tran_monthly_ACCESS_${expname}_1_${year}.nc

#clean up
rm tmp_*

#tsl: soil temperature
ncks -h -O -v tsl $exp.monthly.$year.nc tmp_GCmon_${year}_tsl.nc
ncatted -h -a long_name,tsl,o,c,"Temperature of Soil" tmp_GCmon_${year}_tsl.nc
ncrename -h -d z5_soil,sdepth -v z5_soil,sdepth tmp_GCmon_${year}_tsl.nc tsl_monthly_ACCESS_${expname}_1_${year}.nc

#huss: near-surface specific humidity
ncks -h -O -v huss $exp.monthly.$year.nc tmp_GCmon_${year}_huss.nc
ncatted -h -a long_name,huss,o,c,"Near-Surface Specific Humidity" tmp_GCmon_${year}_huss.nc
ncatted -h -O -a comment,huss,c,c,"reported at 1.5m above surface" tmp_GCmon_${year}_huss.nc \
    huss_monthly_ACCESS_${expname}_1_${year}.nc

#tas: near-surface air temperature
ncks -h -O -v tas $exp.monthly.$year.nc tmp_GCmon_${year}_tas.nc
ncatted -h -a long_name,tas,o,c,"Near-Surface Air Temperature" tmp_GCmon_${year}_tas.nc
ncatted -h -O -a comment,tas,c,c,"reported at 1.5m above surface" tmp_GCmon_${year}_tas.nc \
    tas_monthly_ACCESS_${expname}_1_${year}.nc

#pr: precipitation
ncks -h -O -v pr $exp.monthly.$year.nc tmp_GCmon_${year}_pr.nc
ncatted -h -a long_name,pr,o,c,"Precipitation" tmp_GCmon_${year}_pr.nc \
    pr_monthly_ACCESS_${expname}_1_${year}.nc

#psl: sea level pressure
ncks -h -O -v psl $exp.monthly.$year.nc tmp_GCmon_${year}_psl.nc
ncatted -h -a long_name,psl,o,c,"Sea Level Pressure" tmp_GCmon_${year}_psl.nc \
    psl_monthly_ACCESS_${expname}_1_${year}.nc

#clean up
rm tmp_*

#sfcWind: wind speed at surface
ncks -h -O -v wnd $exp.monthly.$year.nc tmp_GCmon_${year}_sfcWind.nc
ncrename -h -O -v wnd,sfcWind tmp_GCmon_${year}_sfcWind.nc
ncatted -h -a name,sfcWind,o,c,"sfcWind" tmp_GCmon_${year}_sfcWind.nc
ncatted -h -a long_name,sfcWind,o,c,"Near Surface Wind Speed" tmp_GCmon_${year}_sfcWind.nc
ncatted -h -O -a comment,sfcWind,o,c,"reported at 10m above surface on B grid" tmp_GCmon_${year}_sfcWind.nc \
    sfcWind_monthly_ACCESS_${expname}_1_${year}.nc

#rhs: near-surface relative humidity
ncks -h -O -v hurs $exp.monthly.$year.nc tmp_GCmon_${year}_hurs.nc
ncrename -h -O -v hurs,rhs tmp_GCmon_${year}_hurs.nc tmp_GCmon_${year}_rhs.nc
ncatted -h -a name,rhs,o,c,"rhs" tmp_GCmon_${year}_rhs.nc
ncatted -h -a long_name,rhs,o,c,"Near-Surface Relative Humidity" tmp_GCmon_${year}_rhs.nc
ncatted -h -O -a comment,rhs,c,c,"reported at 1.5m above surface" tmp_GCmon_${year}_rhs.nc \
    rhs_monthly_ACCESS_${expname}_1_${year}.nc

#snc: surface snow area fraction -> not available, only snow amount
ncks -O -v snw_land $exp.monthly.$year.nc tmp_GCmon_${year}_snw_land.nc
ncatted -h -a long_name,snw_land,o,c,"Surface Snow Amount over Land" tmp_GCmon_${year}_snw_land.nc \
    snw_land_monthly_ACCESS_${expname}_1_${year}.nc

#clt: cloud fraction
ncks -h -O -v clt $exp.monthly.$year.nc tmp_GCmon_${year}_clt.nc
cdo mulc,100 tmp_GCmon_${year}_clt.nc tmp_GCmon_${year}_clt_prct.nc
ncatted -h -a units,clt,o,c,"%" tmp_GCmon_${year}_clt_prct.nc  
ncatted -h -a long_name,clt,o,c,"Total Cloud Fraction" tmp_GCmon_${year}_clt_prct.nc \
    clt_monthly_ACCESS_${expname}_1_${year}.nc

#tslsi: Surface Temperature where land or sea ice
#need ts, ts_sea (sst from ancillary), sic (from ancillary)
ncks -h -O -v ts $exp.monthly.$year.nc tmp_GCmon_${year}_ts.nc
if  [ $year -le 2006 ]; then
    sst_file=/short/dt6/rzl561/input_data/sst_amip_${year}.nc
else sst_file=/short/dt6/rzl561/input_data/ACCESS1-3_rcp85/tos_Omon_ACCESS1-3_rcp85_r1i1p1_${year}.nc
fi
if  [ $year -le 2006 ]; then
    sic_file=/short/dt6/rzl561/input_data/seaice_amip_${year}.nc
    sic_var=iceconc
else sic_file=/short/dt6/rzl561/input_data/ACCESS1-3_rcp85/sic_OImon_ACCESS1-3_rcp85_r1i1p1_${year}.nc
    sic_var=sic
fi
#land fraction: /home/561/rzl561/input_data/qrparm.landfrac.nc
#land or sea ice fraction
if [ $year == $year_start ]; then
cdo mulc,-1 /home/561/rzl561/input_data/qrparm.landfrac.nc neg_landfrac.nc
cdo addc,1 neg_landfrac.nc not_land.nc
fi
cdo mul not_land.nc $sic_file tmp_sea_ice_frac_${year}.nc
cdo add /home/561/rzl561/input_data/qrparm.landfrac.nc tmp_sea_ice_frac_${year}.nc tmp_land_sea_ice_${year}.nc
#open ocean fraction
cdo mulc,-1 $sic_file tmp_neg_sea_ice_frac_${year}.nc
cdo addc,1 tmp_neg_sea_ice_frac_${year}.nc tmp_not_sea_ice_${year}.nc
cdo mul not_land.nc tmp_not_sea_ice_${year}.nc tmp_open_ocean_frac_${year}.nc
cdo mul $sst_file tmp_open_ocean_frac_${year}.nc tmp_open_ocean_temp_${year}.nc

cdo sub tmp_GCmon_${year}_ts.nc tmp_open_ocean_temp_${year}.nc tmp_ts-sst_${year}.nc
cdo div tmp_ts-sst_${year}.nc tmp_land_sea_ice_${year}.nc tmp_GCmon_${year}_tslsi.nc
cdo ltc,360 tmp_GCmon_${year}_tslsi.nc tslsi_mask_toohigh.nc
cdo setctomiss,0 tslsi_mask_toohigh.nc tslsi_mask_toohighmiss.nc
cdo gtc,170 tmp_GCmon_${year}_tslsi.nc tslsi_mask_toolow.nc
cdo setctomiss,0 tslsi_mask_toolow.nc tslsi_mask_toolowmiss.nc

cdo mul tmp_GCmon_${year}_tslsi.nc tslsi_mask_toohighmiss.nc tmp_GCmon_${year}_tslsi_maskhigh.nc
cdo mul tmp_GCmon_${year}_tslsi_maskhigh.nc tslsi_mask_toolowmiss.nc tmp_GCmon_${year}_tslsi_mask.nc

ncrename -h -O -v ts,tslsi tmp_GCmon_${year}_tslsi_mask.nc
ncatted -h -a name,tslsi,o,c,"tslsi" tmp_GCmon_${year}_tslsi_mask.nc
ncatted -h -a long_name,tslsi,o,c,"Surface Temperature Where Land or Sea Ice" tmp_GCmon_${year}_tslsi_mask.nc
ncatted -h -a standard_name,tslsi,o,c,"surface_temperature" tmp_GCmon_${year}_tslsi_mask.nc
ncatted -h -O -a comment,tslsi,c,c,"tslsi=(ts-ts_sea*A_o)/A_lsi)\n ts=gloabl skin temperature\n ts_sea=sea surface temperature from ancillary file\n A_o=(1-A_l)*(1-sic)) (open ocean fraction)/n A_l=land fraction\n sic=sea ice fraction from ancillary file\n A_lsi=A_l+(1-A_l)*sic (land or sea ice fraction). Unreasonably low and high values set to NaN" \
 tmp_GCmon_${year}_tslsi_mask.nc tslsi_monthly_ACCESS_${expname}_1_${year}.nc

#prc: convective precipitation, prc1+prc2
ncks -h -O -v prc1 $exp.monthly.$year.nc tmp_GCmon_${year}_prc1.nc
ncks -h -O -v prc2 $exp.monthly.$year.nc tmp_GCmon_${year}_prc2.nc
cdo add tmp_GCmon_${year}_prc1.nc tmp_GCmon_${year}_prc2.nc tmp_GCmon_${year}_prc.nc
ncrename -h -O -v prc1,prc tmp_GCmon_${year}_prc.nc
ncatted -h -a name,prc,o,c,"prc" tmp_GCmon_${year}_prc.nc
ncatted -h -a long_name,prc,o,c,"Convective Precipitation" tmp_GCmon_${year}_prc.nc
ncatted -h -a standard_name,prc,o,c,"convective_precipitation_flux" tmp_GCmon_${year}_prc.nc \
    prc_monthly_ACCESS_${expname}_1_${year}.nc

#clean up
rm tmp_*

#hfss: sensible heat flux
ncks -h -O -v hfss $exp.monthly.$year.nc tmp_GCmon_${year}_hfss.nc
ncatted -h -a long_name,hfss,o,c,"Surface Upward Sensible Heat Flux" tmp_GCmon_${year}_hfss.nc \
    hfss_monthly_ACCESS_${expname}_1_${year}.nc

#rlds: surface down longwave
ncks -h -O -v rlds $exp.monthly.$year.nc tmp_GCmon_${year}_rlds.nc
ncatted -h -a long_name,rlds,o,c,"Surface Downwelling Longwave Radiation" tmp_GCmon_${year}_rlds.nc
ncatted -h -a standard_name,rlds,o,c,"surface_downwelling_longwave_flux_in_air" \
    tmp_GCmon_${year}_rlds.nc rlds_monthly_ACCESS_${expname}_1_${year}.nc

#rlus: surface upwelling longwave, LWdown-LWnet
#LWnet:
ncks -h -O -v rls $exp.monthly.$year.nc tmp_GCmon_${year}_rls.nc
#LWup=LWdown-LWnet
cdo sub tmp_GCmon_${year}_rlds.nc tmp_GCmon_${year}_rls.nc tmp_GCmon_${year}_rlus.nc
ncrename -h -O -v rlds,rlus tmp_GCmon_${year}_rlus.nc
ncatted -h -a name,rlus,o,c,"rlus" tmp_GCmon_${year}_rlus.nc
ncatted -h -a long_name,rlus,o,c,"Surface Upwelling Longwave Radiation" tmp_GCmon_${year}_rlus.nc
ncatted -h -a standard_name,rlus,o,c,"surface_upwelling_longwave_flux_in_air" \
    tmp_GCmon_${year}_rlus.nc rlus_monthly_ACCESS_${expname}_1_${year}.nc

#rsds: surface downwelling shortwave
ncks -h -O -v rsds $exp.monthly.$year.nc tmp_GCmon_${year}_rsds.nc
ncatted -h -a long_name,rsds,o,c,"Surface Downwelling Shortwave Radiation" tmp_GCmon_${year}_rsds.nc
ncatted -h -a standard_name,rsds,o,c,"surface_downwelling_shortwave_flux_in_air" \
    tmp_GCmon_${year}_rsds.nc rsds_monthly_ACCESS_${expname}_1_${year}.nc

#rsus: surface upwelling shortwave, SWdown-SWnet
#SWnet
ncks -h -O -v rss $exp.monthly.$year.nc tmp_GCmon_${year}_rss.nc
#SWup=SWdown-SWnet
cdo sub tmp_GCmon_${year}_rsds.nc tmp_GCmon_${year}_rss.nc tmp_GCmon_${year}_rsus.nc
ncrename -h -O -v rsds,rsus tmp_GCmon_${year}_rsus.nc
ncatted -h -a name,rsus,o,c,"rsus" tmp_GCmon_${year}_rsus.nc
ncatted -h -a long_name,rsus,o,c,"Surface Upwelling Shortwave Radiation" tmp_GCmon_${year}_rsus.nc
ncatted -h -a standard_name,rsus,o,c,"surface_upwelling_shortwave_flux_in_air" \
    tmp_GCmon_${year}_rsus.nc rsus_monthly_ACCESS_${expname}_1_${year}.nc

#TOA radiation
ncks -O -v rsdt $exp.monthly.$year.nc tmp_GCmon_${year}_rsdt.nc
ncatted -h -a long_name,rsdt,o,c,"TOA Incident Shortwave Radiation" tmp_GCmon_${year}_rsdt.nc \
    rsdt_monthly_ACCESS_${expname}_1_${year}.nc

ncks -h -O -v rsut $exp.monthly.$year.nc tmp_GCmon_${year}_rsut.nc
ncatted -h -a long_name,rsut,o,c,"TOA Outgoing Shortwave Radiation" tmp_GCmon_${year}_rsut.nc \
    rsut_monthly_ACCESS_${expname}_1_${year}.nc

ncks -h -O -v rlut $exp.monthly.$year.nc tmp_GCmon_${year}_rlut.nc
ncatted -h -a long_name,rlut,o,c,"TOA Outgoing Longwave Radiation" tmp_GCmon_${year}_rlut.nc \
    rlut_monthly_ACCESS_${expname}_1_${year}.nc

#nbp: carbon mass flux
ncks -h -O -v npp $exp.monthly.$year.nc tmp_GCmon_${year}_npp.nc
ncks -h -O -v field3293 $exp.monthly.$year.nc tmp_GCmon_${year}_Rsoi.nc
cdo sub tmp_GCmon_${year}_npp.nc tmp_GCmon_${year}_Rsoi.nc tmp_GCmon_${year}_plusnbp.nc
cdo mulc,-1 tmp_GCmon_${year}_plusnbp.nc tmp_GCmon_${year}_nbp.nc
ncrename -h -O -v npp,nbp tmp_GCmon_${year}_nbp.nc
ncatted -h -a name,nbp,o,c,"nbp" tmp_GCmon_${year}_nbp.nc
ncatted -h -a standard_name,nbp,o,c,"surface_net_downward_mass_flux_of_carbon_dioxide_expresses_as_carbon_due_to_all_land_processes" tmp_GCmon_${year}_nbp.nc
ncatted -h -a long_name,nbp,o,c,"Carbon Mass Flux out of Atmosphere \
due to Net Biospheric Production on Land" tmp_GCmon_${year}_nbp.nc \
    nbp_monthly_ACCESS_${expname}_1_${year}.nc

#clean up
rm tmp_*


let year=year+1
done #year

#concatenate year, zip and move to outdir
ncrcat -h -O mrsos_monthly_ACCESS_${expname}_1_????.nc mrsos_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip mrsos_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv mrsos_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -h -O mrso_monthly_ACCESS_${expname}_1_????.nc mrso_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip mrso_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv mrso_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -h -O mrfso_monthly_ACCESS_${expname}_1_????.nc mrfso_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip mrfso_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv mrfso_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -h -O mrros_monthly_ACCESS_${expname}_1_????.nc mrros_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip mrros_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv mrros_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -h -O mrro_monthly_ACCESS_${expname}_1_????.nc mrro_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip mrro_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv mrro_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -h -O evspsblveg_monthly_ACCESS_${expname}_1_????.nc evspsblveg_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip evspsblveg_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv evspsblveg_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -h -O evspsblsoi_monthly_ACCESS_${expname}_1_????.nc evspsblsoi_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip evspsblsoi_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv evspsblsoi_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -h -O tran_monthly_ACCESS_${expname}_1_????.nc tran_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip tran_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv tran_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O mrlsl_monthly_ACCESS_${expname}_1_????.nc mrlsl_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip mrlsl_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv mrlsl_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -h -O tsl_monthly_ACCESS_${expname}_1_????.nc tsl_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip tsl_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv tsl_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -h -O huss_monthly_ACCESS_${expname}_1_????.nc huss_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip huss_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv huss_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}
#tasmin -> daily
#tasmax -> daily
ncrcat -h -O tas_monthly_ACCESS_${expname}_1_????.nc tas_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip tas_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv tas_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -h -O pr_monthly_ACCESS_${expname}_1_????.nc pr_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip pr_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv pr_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -h -O psl_monthly_ACCESS_${expname}_1_????.nc psl_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip psl_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv psl_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -h -O sfcWind_monthly_ACCESS_${expname}_1_????.nc sfcWind_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip sfcWind_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv sfcWind_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -h -O rhs_monthly_ACCESS_${expname}_1_????.nc rhs_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip rhs_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv rhs_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

#rhsmax -> daily
#rhsmin -> daily
#snc
ncrcat -h -O snw_land_monthly_ACCESS_${expname}_1_????.nc snw_land_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip snw_land_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv snw_land_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -h -O clt_monthly_ACCESS_${expname}_1_????.nc clt_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip clt_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv clt_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -h -O tslsi_monthly_ACCESS_${expname}_1_????.nc tslsi_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip tslsi_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv tslsi_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O prc_monthly_ACCESS_${expname}_1_????.nc prc_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip prc_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv prc_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -h -O hfls_monthly_ACCESS_${expname}_1_????.nc hfls_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip hfls_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv hfls_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -h -O hfss_monthly_ACCESS_${expname}_1_????.nc hfss_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip hfss_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv hfss_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O rlds_monthly_ACCESS_${expname}_1_????.nc rlds_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip rlds_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv rlds_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -h -O rlus_monthly_ACCESS_${expname}_1_????.nc rlus_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip rlus_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv rlus_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -h -O rsds_monthly_ACCESS_${expname}_1_????.nc rsds_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip rsds_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv rsds_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O rsus_monthly_ACCESS_${expname}_1_????.nc rsus_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip rsus_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv rsus_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -h -O rsdt_monthly_ACCESS_${expname}_1_????.nc rsdt_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip rsdt_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv rsdt_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O rsut_monthly_ACCESS_${expname}_1_????.nc rsut_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip rsut_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv rsut_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -h -O rlut_monthly_ACCESS_${expname}_1_????.nc rlut_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip rlut_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv rlut_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -h -O nbp_monthly_ACCESS_${expname}_1_????.nc nbp_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
gzip nbp_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv nbp_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

#clean up
rm ${workdir}/*
