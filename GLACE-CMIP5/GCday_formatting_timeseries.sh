#!/bin/bash
## \file GCmon_formatting_timeseries.sh
#  \author Ruth Lorenz (r.lorenz@unsw.edu.au)
#  \brief daily GLACE output in correct format from GCday_formatting into timeseries
#  \usage qsub -V -v GCday_formatting_timeseries.sh

#PBS -q copyq
#PBS -N GCday_format
#PBS -j oe
#PBS -l other=mdss
#PBS -l ncpus=1
###PBS -l mem=1536mb
#PBS -l walltime=05:00:00
#PBS -l wd

##-----------------------##
## load required modules ##
##-----------------------##

module load netcdf
module load nco
module load cdo
 
##---------------------##
## user specifications ##
##-------------------- ##

exp=uamoa
year_start=1950
year_end=2100
workdir=/short/dt6/$USER/postproc/$exp/work/daily/
outdir=/short/dt6/$USER/postproc/$exp/timeseries/GCday/

#determine final experiment name
if [ $exp == "uamoa" ]; then
    expname=CTL
elif [ $exp == "uamom" ]; then
    expname=GC1A85
elif [ $exp == "uamon" ]; then
    expname=GC1B85
elif [ $exp == "uamol" ]; then
    expname=GC1C85
fi

#infos for global attributes in netcdf
TITLE_RUN="ACCESS1.3b output from GLACE-CMIP5 experiment ${expname}"
SOURCE="ACCESS1.3b (HadGEM3-CABLE2.0), AMIP, N96\n"
INSTITUTION="ARC Centre of Excellence for Climate System Science,\n model run at NCI National Facility, Australia \n"
CONTACT="r.lorenz@unsw.edu.au"

##---------------------##

cd ${workdir}

##-------------------------------------------##
## daily files:                            ##
##-------------------------------------------##

#concatenate year, zip and move to outdir
ncrcat -O mrsos_daily_ACCESS_${expname}_1_????.nc mrsos_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" mrsos_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" mrsos_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" mrsos_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" mrsos_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" mrsos_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip mrsos_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv mrsos_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O mrso_daily_ACCESS_${expname}_1_????.nc mrso_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" mrso_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" mrso_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" mrso_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" mrso_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" mrso_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip mrso_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv mrso_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O mrfso_daily_ACCESS_${expname}_1_????.nc mrfso_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" mrfso_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" mrfso_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" mrfso_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" mrfso_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" mrfso_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip mrfso_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv mrfso_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O mrros_daily_ACCESS_${expname}_1_????.nc mrros_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" mrros_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" mrros_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" mrros_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" mrros_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" mrros_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip mrros_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv mrros_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O mrro_daily_ACCESS_${expname}_1_????.nc mrro_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" mrro_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" mrro_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" mrro_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" mrro_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" mrro_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip mrro_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv mrro_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O evspsblveg_daily_ACCESS_${expname}_1_????.nc evspsblveg_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" evspsblveg_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" evspsblveg_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" evspsblveg_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" evspsblveg_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" evspsblveg_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip evspsblveg_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv evspsblveg_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O evspsblsoi_daily_ACCESS_${expname}_1_????.nc evspsblsoi_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" evspsblsoi_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" evspsblsoi_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" evspsblsoi_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" evspsblsoi_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" evspsblsoi_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip evspsblsoi_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv evspsblsoi_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O tran_daily_ACCESS_${expname}_1_????.nc tran_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" tran_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" tran_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" tran_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" tran_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" tran_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip tran_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv tran_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O mrlsl_daily_ACCESS_${expname}_1_????.nc mrlsl_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" mrlsl_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" mrlsl_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" mrlsl_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" mrlsl_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" mrlsl_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip mrlsl_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv mrlsl_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O tsl_daily_ACCESS_${expname}_1_????.nc tsl_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" tsl_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" tsl_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" tsl_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" tsl_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" tsl_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip tsl_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv tsl_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O huss_daily_ACCESS_${expname}_1_????.nc huss_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" huss_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" huss_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" huss_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" huss_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" huss_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip huss_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv huss_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O tasmin_daily_ACCESS_${expname}_1_????.nc tasmin_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" tasmin_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" tasmin_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" tasmin_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" tasmin_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" tasmin_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip tasmin_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv tasmin_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

#tasmin monthly
ncrcat -O tasmin_monmean_ACCESS_${expname}_1_????.nc tasmin_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" tasmin_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" tasmin_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" tasmin_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" tasmin_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" tasmin_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip tasmin_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv tasmin_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz /short/dt6/$USER/postproc/$exp/timeseries/GCmon/

ncrcat -O tasmax_daily_ACCESS_${expname}_1_????.nc tasmax_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" tasmax_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" tasmax_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" tasmax_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" tasmax_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" tasmax_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip tasmax_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv tasmax_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

#tasmin monthly
ncrcat -O tasmax_monmean_ACCESS_${expname}_1_????.nc tasmax_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" tasmax_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" tasmax_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" tasmax_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" tasmax_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" tasmax_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip tasmax_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv tasmax_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz /short/dt6/$USER/postproc/$exp/timeseries/GCmon/

ncrcat -O tas_daily_ACCESS_${expname}_1_????.nc tas_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" tas_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" tas_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" tas_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" tas_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" tas_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip tas_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv tas_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O pr_daily_ACCESS_${expname}_1_????.nc pr_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" pr_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" pr_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" pr_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" pr_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" pr_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip pr_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv pr_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O psl_daily_ACCESS_${expname}_1_????.nc psl_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" psl_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" psl_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" psl_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" psl_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" psl_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip psl_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv psl_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O sfcWind_daily_ACCESS_${expname}_1_????.nc sfcWind_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" sfcWind_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" sfcWind_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" sfcWind_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" sfcWind_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" sfcWind_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip sfcWind_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv sfcWind_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O rhs_daily_ACCESS_${expname}_1_????.nc rhs_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" rhs_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" rhs_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" rhs_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" rhs_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" rhs_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip rhs_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv rhs_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O rhsmax_daily_ACCESS_${expname}_1_????.nc rhsmax_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" rhsmax_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" rhsmax_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" rhsmax_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" rhsmax_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" rhsmax_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip rhsmax_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv rhsmax_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

#rhsmax monthly
ncrcat -O rhsmax_monmean_ACCESS_${expname}_1_????.nc rhsmax_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" rhsmax_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" rhsmax_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" rhsmax_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" rhsmax_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" rhsmax_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip rhsmax_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv rhsmax_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz /short/dt6/$USER/postproc/$exp/timeseries/GCmon/

ncrcat -O rhsmin_daily_ACCESS_${expname}_1_????.nc rhsmin_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" rhsmin_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" rhsmin_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" rhsmin_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" rhsmin_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" rhsmin_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip rhsmin_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv rhsmin_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

#rhsmin monthly
ncrcat -O rhsmin_monmean_ACCESS_${expname}_1_????.nc rhsmin_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" rhsmin_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" rhsmin_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" rhsmin_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" rhsmin_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" rhsmin_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip rhsmin_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv rhsmin_monthly_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz /short/dt6/$USER/postproc/$exp/timeseries/GCmon/

ncrcat -O snw_land_daily_ACCESS_${expname}_1_????.nc snw_land_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" snw_land_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" snw_land_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" snw_land_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" snw_land_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" snw_land_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip snw_land_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv snw_land_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O clt_daily_ACCESS_${expname}_1_????.nc clt_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" clt_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" clt_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" clt_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" clt_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" clt_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip clt_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv clt_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O tslsi_daily_ACCESS_${expname}_1_????.nc tslsi_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" tslsi_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" tslsi_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" tslsi_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" tslsi_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" tslsi_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip tslsi_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv tslsi_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O prc_daily_ACCESS_${expname}_1_????.nc prc_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" prc_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" prc_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" prc_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" prc_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" prc_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip prc_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv prc_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O hfls_daily_ACCESS_${expname}_1_????.nc hfls_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" hfls_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" hfls_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" hfls_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" hfls_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" hfls_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip hfls_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv hfls_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O hfss_daily_ACCESS_${expname}_1_????.nc hfss_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" hfss_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" hfss_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" hfss_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" hfss_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" hfss_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip hfss_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv hfss_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O rlds_daily_ACCESS_${expname}_1_????.nc rlds_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" rlds_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" rlds_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" rlds_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" rlds_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" rlds_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip rlds_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv rlds_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O rlus_daily_ACCESS_${expname}_1_????.nc rlus_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" rlus_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" rlus_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" rlus_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" rlus_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" rlus_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip rlus_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv rlus_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O rsds_daily_ACCESS_${expname}_1_????.nc rsds_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" rsds_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" rsds_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" rsds_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" rsds_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" rsds_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip rsds_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv rsds_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

ncrcat -O rsus_daily_ACCESS_${expname}_1_????.nc rsus_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
#add global attributes
ncatted -h -O -a title,global,a,c,"${TITLE_RUN}" rsus_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a institution,global,a,c,"${INSTITUTION}" rsus_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a source,global,a,c,"${SOURCE}" rsus_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  contact,global,a,c,"${CONTACT}" rsus_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
ncatted -h -O -a  comment,global,a,c,"Daily timeseries" rsus_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc

gzip rsus_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc
mv rsus_daily_ACCESS_${expname}_1_${year_start}01-${year_end}12.nc.gz ${outdir}

#clean up
#rm ${workdir}/*
